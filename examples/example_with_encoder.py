#!/usr/bin/env python
# coding: utf-8


import numpy as np
import asyncio

import os.path as fs

import torch

from vecindexerclient.vecindexer import VecIndexer
from vecindexerclient.vecindexer import ShardingStrategy
from vecindexerclient.vecindexer import ClusterState
from vecindexerclient.cli import ClientOpts, EncodeAndFaissOpts

from vecindexerclient.helpers import (
    TrainEncoderTask,
    encode_embs,
)


def _process_result(result):
    status = True
    for res in result:
        res_dict = res.dump()
        print(res_dict)
        if 'error' in res_dict:
            status = False
    return status


async def create_coll(vecindexer, coll_id, dimension, index_descripton, estimated_vecs_count):
    create_params = {
        "coll_id": coll_id,
        "index": index_descripton,
        "dimension": dimension,
        "estimated_vecs_count": estimated_vecs_count,
    }
    create_result = await vecindexer.create(create_params)
    return _process_result(create_result)


def _encode(vecs, output_dim, encoder_train_embs_file):
    opts = EncodeAndFaissOpts()
    opts.encoder_config = 'etc/encoder_configs/dcec_default.yml'
    opts.encoders_dir = 'encoders_dir'
    opts.encoder = 'test_encoder.pt'
    opts.encoder_input_dimension = vecs.shape[1]
    opts.encoder_output_dimension = output_dim
    opts.train_embs = encoder_train_embs_file

    tet = TrainEncoderTask(opts, None)
    if not tet():
        raise RuntimeError("Train encoder task failed!")

    encoder_path = fs.join(opts.encoders_dir, opts.encoder)
    assert fs.exists(encoder_path)

    encoder = torch.load(encoder_path)
    encoder.encoder.eval()
    encoder.decoder.eval()

    return encode_embs(opts, vecs, encoder)


async def add_embs_with_encoder(
    vecindexer, coll_id, keys, vecs, encoder_train_embs_file, index_dimension
):
    encoded_vecs = _encode(vecs, index_dimension, encoder_train_embs_file)

    add_params = {
        "coll_id": coll_id,
    }
    add_result = await vecindexer.add(keys=keys, vecs=encoded_vecs, params=add_params)
    return _process_result(add_result)


async def main():
    orig_dimension = 1024
    index_dimension = 256
    coll_id = 200

    add_vecs_count = 1000
    search_vecs_count = 100

    base_embs = np.random.rand(add_vecs_count, orig_dimension).astype('float32')
    encoder_train_embs = base_embs[: int(base_embs.shape[0] / 10)]
    encoder_train_embs_file = '/tmp/test_encoder_train_embs.npz'
    np.savez(encoder_train_embs_file, encoder_train_embs)
    base_ids = [i for i in range(base_embs.shape[0])]

    search_embs = np.random.rand(search_vecs_count, orig_dimension).astype('float32')

    index_description = 'IDMap,HNSW32'

    # expected that instances are running
    cluster_state_file = 'etc/config/my_test_cluster_state.ini'
    opts = ClientOpts()
    opts.verbose = False
    opts.cluster_state_file = cluster_state_file

    vecindexer = VecIndexer(
        opts,
        ClusterState(opts.cluster_state_file),
        ShardingStrategy(),
    )

    if not await create_coll(
        vecindexer, coll_id, index_dimension, index_description, add_vecs_count * 2
    ):
        raise RuntimeError("Failed to create collection!")
    else:
        print("Collection created!")

    if not await add_embs_with_encoder(
        vecindexer, coll_id, base_ids, base_embs, encoder_train_embs_file, index_dimension
    ):
        raise RuntimeError("Failed to add vectors!")
    else:
        print("Vectors added!")

    search_params = {
        "coll_id": coll_id,
    }

    encoded_search_embs = _encode(search_embs, index_dimension, encoder_train_embs_file)

    search_result = await vecindexer.search(vecs=encoded_search_embs, params=search_params)
    print("Search finished!")
    print("Found vectors:", search_result["found"])
    print("Found I shape:", search_result["found_ids"].shape)
    print("Found I(one example):", search_result["found_ids"][:1])
    print("Found D shape:", search_result["found_distances"].shape)
    print("Found D(one example):", search_result["found_distances"][:1])

    await vecindexer.stop()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
