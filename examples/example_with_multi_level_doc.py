#!/usr/bin/env python
# coding: utf-8


import numpy as np
import asyncio

from vecindexerclient.vecindexer import VecIndexer
from vecindexerclient.vecindexer import ShardingStrategy
from vecindexerclient.vecindexer import ClusterState
from vecindexerclient.cli import ClientOpts


def _process_result(result):
    status = True
    for res in result:
        res_dict = res.dump()
        print(res_dict)
        if 'error' in res_dict:
            status = False
    return status


async def create_coll(vecindexer, coll_id, dimension, index_descripton, estimated_vecs_count):
    create_params = {
        "coll_id": coll_id,
        "index": index_descripton,
        "dimension": dimension,
        "estimated_vecs_count": estimated_vecs_count,
    }
    create_result = await vecindexer.create(create_params)
    return _process_result(create_result)


async def add_embs(vecindexer, coll_id, keys, vecs):
    add_params = {
        "coll_id": coll_id,
    }
    add_result = await vecindexer.add(keys=keys, vecs=vecs, params=add_params)
    return _process_result(add_result)


async def add_embs_ml(vecindexer, coll_id, keys, vecs):
    add_params = {"coll_id": coll_id, "multi_level": True}
    add_result = await vecindexer.add_multi_level(keys=keys, vecs=vecs, params=add_params)
    return _process_result(add_result)


async def main():
    dimension = 1024
    coll_id = 300

    add_vecs_count = 1000
    search_vecs_count = 100

    base_embs = np.random.rand(add_vecs_count, dimension).astype('float32')
    add_ids_with_levels = []
    level1 = []
    level2 = []
    level3 = []
    for i in range(1, add_vecs_count + 1):
        level1.append(int(i / 4) + add_vecs_count * 1.5)
        level2.append(int(i / 2) + add_vecs_count)
        level3.append(i)

    add_ids_with_levels.extend(level1)
    add_ids_with_levels.extend(level2)
    add_ids_with_levels.extend(level3)

    assert len(add_ids_with_levels) == add_vecs_count * 3

    search_embs = np.random.rand(search_vecs_count, dimension).astype('float32')

    index_description = 'IDMap,HNSW32'
    # expected that instances are running
    cluster_state_file = 'etc/config/my_test_cluster_state.ini'
    opts = ClientOpts()
    opts.verbose = False
    opts.cluster_state_file = cluster_state_file

    vecindexer = VecIndexer(
        opts,
        ClusterState(opts.cluster_state_file),
        ShardingStrategy(),
    )

    if not await create_coll(vecindexer, coll_id, dimension, index_description, add_vecs_count * 2):
        raise RuntimeError("Failed to create collection!")
    else:
        print("Collection created!")

    if not await add_embs_ml(vecindexer, coll_id, add_ids_with_levels, base_embs):
        raise RuntimeError("Failed to add vectors with multi-level!")
    else:
        print("Vectors with multi level id table added!")

    search_params = {
        "coll_id": coll_id,
        "parents": True,
    }
    search_result = await vecindexer.search_multi_level(vecs=search_embs, params=search_params)
    print("Search finished!")
    print("Found vectors:", search_result["found"])
    print("Found parent documents(shape):", search_result["parents"].shape)
    print("Found parent documents(one example):", search_result["parents"][0])
    print("Found I shape:", search_result["found_ids"].shape)
    print("Found I(one example):", search_result["found_ids"][:1])
    print("Found D shape:", search_result["found_distances"].shape)
    print("Found D(one example):", search_result["found_distances"][:1])

    await vecindexer.stop()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
