#!/usr/bin/env python
# coding: utf-8


import numpy as np
import asyncio

from vecindexerclient.vecindexer import VecIndexer
from vecindexerclient.vecindexer import ShardingStrategy
from vecindexerclient.vecindexer import ClusterState
from vecindexerclient.cli import ClientOpts


def _process_result(result):
    status = True
    for res in result:
        res_dict = res.dump()
        print(res_dict)
        if 'error' in res_dict:
            status = False
    return status


async def create_coll(
    vecindexer, coll_id, dimension, index_descripton, estimated_vecs_count, use_gpu
):
    create_params = {
        "coll_id": coll_id,
        "index": index_descripton,
        "dimension": dimension,
        "estimated_vecs_count": estimated_vecs_count,
        "use_gpu": use_gpu,
    }
    create_result = await vecindexer.create(create_params)
    return _process_result(create_result)


async def train(vecindexer, coll_id, vecs):
    train_params = {
        "coll_id": coll_id,
    }
    train_result = await vecindexer.train(vecs=vecs, params=train_params)
    return _process_result(train_result)


async def add_embs(vecindexer, coll_id, keys, vecs):
    add_params = {
        "coll_id": coll_id,
    }
    add_result = await vecindexer.add(keys=keys, vecs=vecs, params=add_params)
    return _process_result(add_result)


async def main():
    dimension = 1024
    coll_id = 400

    add_vecs_count = 20000
    train_vecs_count = 10000
    search_vecs_count = 10000

    train_embs = np.random.rand(train_vecs_count, dimension).astype('float32')

    base_embs = np.random.rand(add_vecs_count, dimension).astype('float32')
    base_ids = [i for i in range(base_embs.shape[0])]

    search_embs = np.random.rand(search_vecs_count, dimension).astype('float32')

    index_description = 'IDMap,IVF256,PQ64x8'
    # expected that instances are running
    cluster_state_file = 'etc/config/my_test_cluster_state.ini'
    opts = ClientOpts()
    opts.verbose = False
    opts.cluster_state_file = cluster_state_file

    vecindexer = VecIndexer(
        opts,
        ClusterState(opts.cluster_state_file),
        ShardingStrategy(),
    )

    if not await create_coll(
        vecindexer, coll_id, dimension, index_description, add_vecs_count * 2, use_gpu=True
    ):
        raise RuntimeError("Failed to create collection!")
    else:
        print("Collection created!")

    if not await train(vecindexer, coll_id, train_embs):
        raise RuntimeError("Failed to train!")
    else:
        print("Trained!")

    if not await add_embs(vecindexer, coll_id, base_ids, base_embs):
        raise RuntimeError("Failed to add vectors!")
    else:
        print("Vectors added!")

    search_params = {
        "coll_id": coll_id,
    }
    search_result = await vecindexer.search(vecs=search_embs, params=search_params)
    print("Search finished!")
    print("Found vectors:", search_result["found"])
    print("Found I shape:", search_result["found_ids"].shape)
    print("Found I(one example):", search_result["found_ids"][:1])
    print("Found D shape:", search_result["found_distances"].shape)
    print("Found D(one example):", search_result["found_distances"][:1])

    await vecindexer.stop()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
