#!/usr/bin/env python
# coding: utf-8

import array
import asyncio
import copy
import numpy as np
import pytest
import pytest_asyncio
import uvloop
import zmq.asyncio
import logging

import vecindexerclient.async_client as async_client
from vecindexerclient.cluster_state import ClusterStateFromFileConfig as ClusterState
from vecindexerclient.sharding_strategy import ShardingStrategy

from vecindexerclient.vecindexer import generate_req_id
from vecindexerclient.vecindexer import VecIndexer
from vecindexerclient.cli import (
    CreateCollectionOpts,
    create_coll_cli,
    InfoOpts,
    info_cli,
    SearchOpts,
    search_cli,
)
from vecindexerclient.response import Response, SearchResponse

from .mock_server import ServerOpts, MockServer

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


class BaseClientOpts:
    def __init__(self, transport='inproc', endpoint='server'):
        self.transport = transport
        self.endpoint = endpoint


class ClientOpts(BaseClientOpts):
    def __init__(self, transport='inproc', endpoint='server', array_type='numpy'):
        super(ClientOpts, self).__init__(transport, endpoint)
        self.array_type = array_type


@pytest.fixture
def zmq_ctx(event_loop):
    return zmq.asyncio.Context()


@pytest_asyncio.fixture
async def server_fixture(request, zmq_ctx):
    opts = ServerOpts()
    # request.cls.test_name
    server = MockServer(opts, zmq_ctx)
    asyncio.create_task(server.run())
    yield server
    await server.stop()


@pytest.fixture
def sharding_strategy_fixture():
    return ShardingStrategy()


@pytest.fixture
def cluster_state_fixture():
    config_file_path = "tests/etc/config/test_cluster_state.ini"
    cluster_state = ClusterState(config_file_path)
    return cluster_state


@pytest_asyncio.fixture
async def shards_fixture(request, cluster_state_fixture, zmq_ctx):
    shard_endpoints = cluster_state_fixture.get_shard_endpoints(return_leaders=True)
    logging.debug("shard_endpoints: %s", shard_endpoints)

    assert len(shard_endpoints) >= 3

    shard_servers = []
    for endpoint in shard_endpoints:
        shard_opts = ServerOpts(endpoint=endpoint)
        shard_servers.append(MockServer(shard_opts, zmq_ctx))

    for server in shard_servers:
        asyncio.create_task(server.run())

    yield shard_servers

    for server in shard_servers:
        await server.stop()


class App:
    def __init__(self, server, zmq_ctx):
        self.server = server
        self.zmq_ctx = zmq_ctx


class ShardApp:
    def __init__(self, shards, zmq_ctx):
        self.shards = shards
        self.zmq_ctx = zmq_ctx


@pytest.fixture
def app(server_fixture, zmq_ctx):
    return App(server_fixture, zmq_ctx)


@pytest.fixture
def shard_app(shards_fixture, zmq_ctx):
    return ShardApp(shards_fixture, zmq_ctx)


class TestWithAsyncClient:
    @pytest_asyncio.fixture
    async def client(self, app, request):
        cl = async_client.AsyncClient(ClientOpts(), zmq_ctx=app.zmq_ctx)
        yield cl
        await cl.stop()

    @pytest_asyncio.fixture
    async def client_with_default_array(self, app, request):
        cl = async_client.AsyncClient(ClientOpts(array_type='array'), zmq_ctx=app.zmq_ctx)
        yield cl
        await cl.stop()


class TestWithShardedClient:
    @pytest_asyncio.fixture
    async def sharded_client(self, shard_app, request):
        cl = async_client.ShardedAsyncClient(
            ClientOpts(), shard_app.shards, zmq_ctx=shard_app.zmq_ctx
        )
        yield cl
        await cl.stop()

    @pytest_asyncio.fixture
    async def vecindexer_client(
        self, shard_app, cluster_state_fixture, sharding_strategy_fixture, request
    ):
        vecindexer = VecIndexer(
            ClientOpts(),
            cluster_state_fixture,
            sharding_strategy_fixture,
            zmq_ctx=shard_app.zmq_ctx,
        )
        yield vecindexer
        await vecindexer.stop()


class TestApiBaseCommands(TestWithAsyncClient):
    test_name = 'test_api_base_commands'

    @pytest.mark.asyncio
    async def test_simple(self, client):
        response = await client.make_request(
            'test_return_ok',
            req_no=1000,
            params={'test params key': 'test params value'},
        )
        assert response.req_no == 1000
        assert response.result['status'] == 'OK'

    @pytest.mark.asyncio
    async def test_echo(self, client):
        response = await client.make_request(
            'test_echo',
            req_no=2000,
            params={'test echo params key': 'test echo params value'},
        )
        assert response.req_no == 2000
        assert response.result['status'] == 'OK'
        assert response.result['result'] == {'test echo params key': 'test echo params value'}

    @pytest.mark.asyncio
    async def test_unknown_command(self, client):
        response = await client.make_request(
            'an_unknown_command',
            req_no=3000,
            params={'k': 'v'},
        )
        assert response.req_no == 3000
        assert 'status' not in response.result
        assert 'result' not in response.result
        assert response.result['error'].startswith("Unknown cmd:")

    async def _test_echo_numpy_array(self, x, client, req_no):
        x = copy.copy(x)
        params = {
            'np_array_x': x,
        }
        response = await client.make_request(
            'test_echo',
            req_no=req_no,
            params=params,
        )
        assert response.req_no == req_no
        assert response.result['status'] == 'OK'

        got_x = response.result['result']['np_array_x']
        np.testing.assert_array_equal(x, got_x, strict=True)

    @pytest.mark.asyncio
    async def test_echo_numpy_array_1d(self, client):
        n = 100
        rng = np.random.default_rng(1234)
        x = rng.random((n,)).astype('float32')

        assert x.shape == (100,)
        assert str(x.dtype) == 'float32'

        await self._test_echo_numpy_array(x=x, client=client, req_no=4001)

    @pytest.mark.asyncio
    async def test_echo_numpy_array_1d_float16(self, client):
        n = 100
        rng = np.random.default_rng(1234)
        x = rng.random((n,)).astype('float16')

        assert x.shape == (100,)
        assert str(x.dtype) == 'float16'

        await self._test_echo_numpy_array(x=x, client=client, req_no=4002)

    @pytest.mark.asyncio
    async def test_echo_numpy_array_1d_int64(self, client):
        n = 100
        rng = np.random.default_rng(1234)
        x = rng.random((n,)).astype('int64')

        assert x.shape == (100,)
        assert str(x.dtype) == 'int64'

        await self._test_echo_numpy_array(x=x, client=client, req_no=4003)

    @pytest.mark.asyncio
    async def test_echo_vectors_2d(self, client):
        d = 8
        n = 100
        rng = np.random.default_rng(1234)
        x = rng.random((n, d)).astype('float32')
        x[:, 0] += np.arange(n) / 1000.0

        assert x.shape == (100, 8)
        assert str(x.dtype) == 'float32'

        await self._test_echo_numpy_array(x=x, client=client, req_no=4101)

    @pytest.mark.asyncio
    async def test_echo_vectors_2d_float16(self, client):
        d = 8
        n = 100
        rng = np.random.default_rng(1234)
        x = rng.random((n, d)).astype('float16')
        x[:, 0] += np.arange(n) / 1000.0

        assert x.shape == (100, 8)
        assert str(x.dtype) == 'float16'

        await self._test_echo_numpy_array(x=x, client=client, req_no=4102)

    @pytest.mark.asyncio
    async def test_echo_vectors_2d_int64(self, client):
        d = 8
        n = 100
        rng = np.random.default_rng(1234)
        x = rng.random((n, d)).astype('int64')

        assert x.shape == (100, 8)
        assert str(x.dtype) == 'int64'

        await self._test_echo_numpy_array(x=x, client=client, req_no=4103)

    @pytest.mark.asyncio
    async def _test_echo_big_numpy_array(self, client, count, req_no):
        """
        example based on https://github.com/facebookresearch/faiss/wiki/Getting-started
        """
        d = 64  # dimension
        nb = count  # database size
        nq = int(count / 10)  # nb of queries
        rng = np.random.default_rng(1234)
        xb = rng.random((nb, d)).astype('float32')
        xb[:, 0] += np.arange(nb) / 1000.0
        xq = rng.random((nq, d)).astype('float32')
        xq[:, 0] += np.arange(nq) / 1000.0

        params = {
            'np_array_xb': xb,
            'np_array_xq': xq,
        }
        response = await client.make_request(
            'test_echo',
            req_no=req_no,
            params=params,
        )
        assert response.req_no == req_no
        assert response.result['status'] == 'OK'
        assert response.result['result']['np_array_xb'].shape == xb.shape
        assert response.result['result']['np_array_xq'].shape == xq.shape
        assert response.result['result']['np_array_xq'].shape == xq.shape

    @pytest.mark.asyncio
    async def test_echo_vectors_25MiB(self, client):
        """
        About 25 MiB data send to server and get back.
        """
        await self._test_echo_big_numpy_array(client=client, count=100_100, req_no=5001)

    @pytest.mark.skip(reason="Enable this if you need check serializator throughput")
    @pytest.mark.asyncio
    async def test_echo_vectors_250MiB(self, client):
        """
        About 250 MiB data send to server and get back.
        """
        await self._test_echo_big_numpy_array(client=client, count=1_000_000, req_no=5002)

    @pytest.mark.skip(reason="Enable this if you need check serializator throughput")
    @pytest.mark.asyncio
    async def test_echo_vectors_1GiB(self, client):
        """
        About 1 GiB data send to server and get back.
        """
        await self._test_echo_big_numpy_array(client=client, count=4_000_000, req_no=5003)

    @pytest.mark.asyncio
    async def test_echo_default_array(self, client_with_default_array):
        """
        Send array.array and get back. Mostly for serializator checking.
        """
        req_no = 6001
        x = array.array('d', [1.0, 2.0, 3.0, -4, 100])
        params = {'array_x': x, 'string key': 'string value'}
        response = await client_with_default_array.make_request(
            'test_echo',
            req_no=req_no,
            params=params,
        )
        assert response.req_no == req_no
        assert response.result['status'] == 'OK'

        assert params == response.result['result']


class TestShardedServers(TestWithShardedClient):
    @pytest.mark.asyncio
    async def test_sharded_server_functionality(self, shard_app):
        for idx, shard in enumerate(shard_app.shards):

            params = {'shard_test_key': idx}
            opts = ClientOpts(
                transport=shard.get_opts().transport,
                endpoint=shard.get_opts().endpoint,
            )
            client = async_client.AsyncClient(opts, zmq_ctx=shard_app.zmq_ctx)

            response = await client.make_request('test_echo', req_no=1000 + idx, params=params)

            assert response.req_no == 1000 + idx
            assert response.result['status'] == 'OK'
            assert response.result['result'] == params

            await client.stop()

    @pytest.mark.asyncio
    async def test_sharded_server_echo_vectors(self, shard_app):
        for idx, shard in enumerate(shard_app.shards):
            n = 100 + idx
            rng = np.random.default_rng(1234 + idx)
            x = rng.random((n,)).astype('float32')

            params = {'np_array_x': x}

            opts = ClientOpts(
                transport=shard.get_opts().transport,
                endpoint=shard.get_opts().endpoint,
            )
            client = async_client.AsyncClient(opts, zmq_ctx=shard_app.zmq_ctx)

            response = await client.make_request('test_echo', req_no=2000 + idx, params=params)

            assert response.req_no == 2000 + idx
            assert response.result['status'] == 'OK'
            np.testing.assert_array_equal(x, response.result['result']['np_array_x'], strict=True)

            await client.stop()

    @pytest.mark.asyncio
    async def test_sharded_client_make_common_sharded_request(self, vecindexer_client):
        results = await vecindexer_client.make_common_sharded_request(
            'test_echo',
            1111,
            {'test_key': 'test_value'},
        )
        assert len(results) >= 3

        for res in results:
            assert res.req_no == 1111
            assert res.result['status'] == 'OK'
            assert res.result['result']['test_key'] == 'test_value'

    @pytest.mark.asyncio
    async def test_sharded_client_make_sharded_request(self, vecindexer_client):

        shape = (10, 10)
        rng = np.random.default_rng(1234)
        keys = rng.choice(1000, size=shape[0], replace=False).astype('int64')
        vecs = rng.random(shape).astype('float32')

        shard_endpoints = vecindexer_client.get_cluster_instances()
        shard_indices = vecindexer_client.sharding_strategy.shard_for_keys(
            keys, len(shard_endpoints)
        )
        shard_requests = {}
        for keys_idx, vec_idx, shard_idx in zip(
            range(keys.shape[0]), range(vecs.shape[0]), shard_indices
        ):
            if shard_idx not in shard_requests:
                shard_requests[shard_idx] = {'keys_idx': [], 'vec_idx': []}
            shard_requests[shard_idx]['keys_idx'].append(keys_idx)
            shard_requests[shard_idx]['vec_idx'].append(vec_idx)

        def _shard_options_generator():
            for shard_idx, shard_req in shard_requests.items():
                shard_params = dict()

                if isinstance(shard_req['keys_idx'], np.ndarray):
                    shard_params['np_array_keys'] = keys[shard_req['keys_idx']]
                elif isinstance(shard_req['keys_idx'], list):
                    shard_params['np_array_keys'] = np.array(
                        [keys[i] for i in shard_req['keys_idx']], np.uint64
                    )
                else:
                    raise RuntimeError("Incorrect keys_idx!")

                assert shard_params['np_array_keys'].dtype == np.uint64

                shard_params['np_array_vecs'] = vecs[shard_req['vec_idx']]

                assert (
                    shard_params['np_array_keys'].shape[0] == shard_params['np_array_vecs'].shape[0]
                )

                yield generate_req_id(), shard_params, shard_endpoints[shard_idx]

        shard_requests = [req for req in _shard_options_generator()]

        # Save orig requests due to that request is changing in make_request!
        shard_requests_copy = copy.deepcopy(shard_requests)

        results = await vecindexer_client.make_sharded_request(
            'test_echo',
            shard_requests,
            shard_endpoints,
        )

        assert len(results) >= 3

        reqno2req = {r[0]: r for r in shard_requests_copy}
        reqno2res = {r.req_no: r for r in results}

        for reqno in reqno2req.keys():
            req_params = reqno2req[reqno][1]
            res = reqno2res[reqno]
            assert res.result['status'] == 'OK'
            assert req_params.keys() == res.result['result'].keys()
            np.testing.assert_array_equal(
                req_params['np_array_keys'], res.result['result']['np_array_keys'], strict=True
            )
            np.testing.assert_array_equal(
                req_params['np_array_vecs'], res.result['result']['np_array_vecs'], strict=True
            )

    @pytest.mark.asyncio
    async def test_info_base_structure(self, vecindexer_client):
        info_opts = InfoOpts()
        for info_result in await info_cli(vecindexer_client, info_opts):
            assert isinstance(info_result, Response)
            info_result = info_result.dump()
            assert "result" in info_result
            assert "msg" in info_result["result"] and info_result["result"]["msg"] == "OK"
            assert "collections_info" in info_result["result"]
            assert len(info_result["result"]["collections_info"]) > 0
            for collection_info in info_result["result"]["collections_info"]:
                for digit_key in ["coll_id", "dimension", "count"]:
                    assert digit_key in collection_info and (
                        isinstance(collection_info[digit_key], int)
                        or collection_info[digit_key].isigit()
                    )

                assert "trained" in collection_info and isinstance(collection_info["trained"], bool)

            assert "sysinfo" in info_result["result"]
            sysinfo = info_result["result"]["sysinfo"]
            for digit_key in ["total_ram_bytes", "free_ram_bytes", "proc_count", "omp_threads"]:
                assert digit_key in sysinfo and (
                    isinstance(sysinfo[digit_key], int) or sysinfo[digit_key].isigit()
                )

    @pytest.mark.asyncio
    async def test_create_success_case(self, vecindexer_client):
        create_opts = CreateCollectionOpts()
        create_opts.coll_id = 12345
        create_opts.dimension = 1024
        create_opts.index = 'IDMap,HNSW32'
        create_opts.estimated_vecs_count = 10_000
        for create_result in await create_coll_cli(vecindexer_client, create_opts):
            assert isinstance(create_result, Response)
            create_result = create_result.dump()
            result = create_result["result"]
            assert "msg" in result and result["msg"] == "OK"
            assert "copies" in result and result["copies"] >= 1

    @pytest.mark.asyncio
    async def test_add_success_case(self, vecindexer_client):
        params = {
            "coll_id": 12345,
        }

        # From faiss wiki Getting-started
        d = 64
        nb = 1000
        np.random.seed(1234)
        xb = np.random.random((nb, d)).astype('float32')
        xb[:, 0] += np.arange(nb) / 1000.0

        keys = [i for i in range(xb.shape[0])]

        assert len(keys) == xb.shape[0]

        res = await vecindexer_client.add(
            keys=keys,
            vecs=xb,
            params=params,
        )

        inserted_vecs = 0
        for add_result in res:
            assert isinstance(add_result, Response)
            add_result = add_result.dump()
            result = add_result['result']
            assert "msg" in result and result["msg"] == "OK"
            assert "copies" in result and result["copies"] >= 1
            inserted_vecs += result["inserted_vecs"]
            assert "normalize_L2" in result and isinstance(result["normalize_L2"], bool)

        assert inserted_vecs == len(keys)

    @pytest.mark.asyncio
    async def test_search_success_case(self, vecindexer_client):
        params = {
            "coll_id": 12345,
        }

        # From faiss wiki Getting-started
        d = 64
        nq = 100
        np.random.seed(1234)
        xq = np.random.random((nq, d)).astype('float32')
        xq[:, 0] += np.arange(nq) / 1000.0

        res = await vecindexer_client.search(
            vecs=xq,
            params=params,
        )

        assert ('found' in res) and (res['found'] == xq.shape[0])
        assert ('found_ids' in res) and (len(res['found_ids'].shape) == 2)
        assert res['found_ids'].shape[0] == xq.shape[0]
        assert res['found_ids'].shape[1] == 10  # default topk!

        assert ('found_distances' in res) and (len(res['found_distances'].shape) == 2)

        assert res['found_distances'].shape[0] == xq.shape[0]
        assert res['found_distances'].shape[1] == 10  # default topk!

    @pytest.mark.asyncio
    async def test_add_collid_failed(self, vecindexer_client):
        params = {
            "coll_id": 0,
        }

        # From faiss wiki Getting-started
        d = 64
        nb = 1000
        np.random.seed(1234)
        xb = np.random.random((nb, d)).astype('float32')
        xb[:, 0] += np.arange(nb) / 1000.0

        keys = [i for i in range(xb.shape[0])]

        assert len(keys) == xb.shape[0]

        res = await vecindexer_client.add(
            keys=keys,
            vecs=xb,
            params=params,
        )

        inserted_vecs = 0
        for add_result in res:
            assert isinstance(add_result, Response)
            add_result = add_result.dump()
            error = add_result['error']
            assert "msg" in error and error["msg"] == "incorrect coll_id"

    @pytest.mark.asyncio
    async def test_create_collid_failed(self, vecindexer_client):
        create_opts = CreateCollectionOpts()
        create_opts.coll_id = 0
        create_opts.dimension = 1024
        create_opts.index = 'IDMap,HNSW32'
        create_opts.estimated_vecs_count = 10_000
        for create_result in await create_coll_cli(vecindexer_client, create_opts):
            assert isinstance(create_result, Response)
            create_result = create_result.dump()
            error = create_result["error"]
            assert "msg" in error and error["msg"] == "incorrect coll_id"

    @pytest.mark.asyncio
    async def test_create_dimension_failed(self, vecindexer_client):
        create_opts = CreateCollectionOpts()
        create_opts.coll_id = 12345
        create_opts.dimension = 0
        create_opts.index = 'IDMap,HNSW32'
        create_opts.estimated_vecs_count = 10_000
        for create_result in await create_coll_cli(vecindexer_client, create_opts):
            assert isinstance(create_result, Response)
            create_result = create_result.dump()
            error = create_result["error"]
            assert "msg" in error and error["msg"] == "incorrect dimension"

    @pytest.mark.asyncio
    async def test_search_collid_failed(self, vecindexer_client):
        params = {
            "coll_id": 0,
        }

        # From faiss wiki Getting-started
        d = 64
        nq = 100
        np.random.seed(1234)
        xq = np.random.random((nq, d)).astype('float32')
        xq[:, 0] += np.arange(nq) / 1000.0

        res = await vecindexer_client.search(
            vecs=xq,
            params=params,
        )

        assert res['found_distances'] == 0
        assert res['found'] == 0
        assert res['found_ids'] == 0

        search_results = res['raw_results']
        for search_result in search_results:
            assert isinstance(search_result, Response)
            search_result = search_result.dump()
            error = search_result['error']
            assert "msg" in error and error["msg"] == "incorrect coll_id"

    @pytest.mark.asyncio
    async def test_search_topk_failed(self, vecindexer_client):
        params = {
            "coll_id": 12345,
            "topk": 0,
        }

        # From faiss wiki Getting-started
        d = 64
        nq = 100
        np.random.seed(1234)
        xq = np.random.random((nq, d)).astype('float32')
        xq[:, 0] += np.arange(nq) / 1000.0

        res = await vecindexer_client.search(
            vecs=xq,
            params=params,
        )

        assert res['found_distances'] == 0
        assert res['found'] == 0
        assert res['found_ids'] == 0

        search_results = res['raw_results']
        for search_result in search_results:
            assert isinstance(search_result, Response)
            search_result = search_result.dump()
            error = search_result['error']
            assert "msg" in error and error["msg"] == "incorrect topk"
