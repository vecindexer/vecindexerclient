#!/usr/bin/env python
# coding: utf-8

import copy
import numpy as np
import pytest

from vecindexerclient.request_response_convention import ensure_deserialized
from vecindexerclient.request_response_convention import ensure_serialized


@pytest.fixture
def data():
    def _gen_data(seed=1234):
        d = 2  # dimension
        n = 8  # nb of queries
        rng = np.random.default_rng(seed)
        x = rng.random((n, d)).astype('float32')
        x[:, 0] += np.arange(n) / 1000.0
        return x

    d = {
        'numpy_array': _gen_data(),
        'numpy_array2': _gen_data(1),
        "<class 'numpy.ndarray'>": _gen_data(2),
        'numpy.ndarray': _gen_data(3),
        'np.ndarray': _gen_data(4),
        'np.array': _gen_data(5),
        'numpy.array': _gen_data(6),
        'numpy_array_3': _gen_data(7),
        'ndarray': _gen_data(8),
        'np_ndarray': _gen_data(9),
        'numpy_ndarray': _gen_data(10),
        'numpy_array_and_any_tail_here': _gen_data(100),
    }
    return d


@pytest.fixture
def data_serialized(data):
    x = data['numpy_array']

    d = {
        'numpy_array': {'array': x, 'shape': x.shape, 'type': str(x.dtype)},
    }
    return d


def test_serialize(data):
    shape = data['numpy_array'].shape
    dtype = str(data['numpy_array'].dtype)

    data = copy.copy(data)
    serialized = ensure_serialized(data)

    assert 'numpy_array' in serialized
    assert 'shape' in serialized['numpy_array']
    assert 'array' in serialized['numpy_array']
    assert serialized['numpy_array']['array'].shape == shape
    assert str(serialized['numpy_array']['array'].dtype) == dtype


def test_twice_serialize(data):
    data = copy.copy(data)

    serialized_first = ensure_serialized(data)
    serialized_second = ensure_serialized(serialized_first)

    np.testing.assert_array_equal(serialized_first, serialized_second, strict=True)


def test_deserialize(data_serialized):
    shape = data_serialized['numpy_array']['shape']
    dtype = data_serialized['numpy_array']['type']

    data = copy.copy(data_serialized)
    deserialized = ensure_deserialized(data)

    assert isinstance(deserialized['numpy_array'], (np.ndarray,))
    assert deserialized['numpy_array'].shape == shape
    assert str(deserialized['numpy_array'].dtype) == dtype


def test_twice_deserialize(data_serialized):
    data = copy.copy(data_serialized)
    deserialized_first = ensure_deserialized(data)
    deserialized_second = ensure_deserialized(deserialized_first)

    np.testing.assert_array_equal(deserialized_first, deserialized_second, strict=True)


def test_serialize_deserialize(data):
    data = copy.copy(data)

    serialized = ensure_serialized(data)
    deserialized = ensure_deserialized(serialized)

    np.testing.assert_array_equal(data, deserialized, strict=True)


def test_twice_serialize_deserialize(data):
    data = copy.copy(data)

    serialized = ensure_serialized(data)
    deserialized = ensure_deserialized(serialized)

    np.testing.assert_array_equal(data, deserialized, strict=True)

    serialized = ensure_serialized(deserialized)
    deserialized = ensure_deserialized(serialized)

    np.testing.assert_array_equal(data, deserialized, strict=True)
