#!/usr/bin/env python
# coding: utf-8

import asyncio
from io import BytesIO
import faiss
import uvloop
import logging
import zmq.asyncio

from vecindexerclient.request import Request
from vecindexerclient.response import Response

from vecindexerclient.utils import TimeProfiler

from vecindexerclient.cbor2_extra_serializer import Cbor2ExtraSerializer as Serializer

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


class ServerOpts:
    def __init__(self, transport='inproc', endpoint='server'):
        self.transport = transport
        self.endpoint = endpoint


class MockServer:
    def __init__(self, opts, zmq_ctx):
        self._opts = opts
        self._zmq_ctx = zmq_ctx

        self._socket = self._zmq_ctx.socket(zmq.ROUTER)
        self._socket.bind("%s://%s" % (opts.transport, opts.endpoint))

        self._recv_task = None

    def get_opts(self):
        return self._opts

    async def _process_default_request(self, client, empty, cmd, ebuf, raw_request, pending_tasks):
        client = client.bytes

        if cmd == b'quit':
            logging.debug("new request from '%s'\ncmd '%s'", client, cmd.decode('utf-8'))
            await self._socket.send_multipart([client, empty, b"ok"])
            self._recv_task = None
            return None

        with TimeProfiler(
            f"MockServer: loading request '{client}', cmd '{cmd}', len {len(raw_request)}"
        ):
            request = Request.load_from_buffer(ebuf, self._opts)

        req_no = request.req_no

        logging.debug(
            "new default request from '%s'\ncmd '%s'\nrequest[:300]: '%s'",
            client,
            cmd.decode('utf-8'),
            str(request)[:300],
        )

        if cmd == b'test_return_ok':
            response = Response(
                req_no,
                result={
                    'status': 'OK',
                },
            )
        elif cmd == b'test_echo':
            response = Response(
                req_no,
                result={
                    'status': 'OK',
                    'result': request.params,
                },
            )
        else:
            response = Response(
                req_no,
                result={'error': 'Unknown cmd: %s' % cmd.decode('utf-8')},
            )

        await self._socket.send_multipart(
            [
                client,
                empty,
                Response.serialize(response),
            ]
        )

        for t in pending_tasks:
            logging.debug("pending task: %s" % t)

        return None

    async def _handle_recv_task(self, pending_tasks):
        assert self._recv_task is not None, "received response is None (logic error)"

        recieved_task = await self._recv_task
        try:
            client, empty, cmd, raw_request = recieved_task
            cmd = cmd.bytes
            ebuf = BytesIO(raw_request)
            return await self._process_default_request(
                client, empty, cmd, ebuf, raw_request, pending_tasks
            )
        except ValueError as verr:  # handle request with ebuf (like real-server)
            client, empty, raw_request = recieved_task
            ebuf = BytesIO(raw_request)

            method = Serializer.deserialize(ebuf)
            req_no = Serializer.deserialize(ebuf)

        logging.debug("new request req_no '%s' method '%s'", req_no, method)

        response_ebuf = BytesIO()

        def create_response(req_no, error=None, result=None):
            if error is not None:
                response = Response(req_no=req_no, error=error)
            if result is not None:
                response = Response(req_no=req_no, result=result)
            response_ebuf.write(Response.serialize(response))

        if method == 'search':
            vector_count = Serializer.deserialize(ebuf)
            vectors = Serializer.deserialize_np_array(ebuf)
            dimension = int(vectors.shape[0] / vector_count)
            vectors = vectors.reshape(vector_count, dimension)
            params = Serializer.deserialize(ebuf)

            if params.get("coll_id", 0) <= 0:
                error = {'msg': 'incorrect coll_id'}
                create_response(req_no=req_no, error=error)
            elif params.get("topk", 10) <= 0:
                error = {'msg': 'incorrect topk'}
                create_response(req_no=req_no, error=error)

            else:

                index = faiss.index_factory(dimension, "HNSW32", faiss.METRIC_INNER_PRODUCT)
                index.add(vectors)
                D, I = index.search(vectors, k=params.get("topk", 10))

                assert D.shape[0] == I.shape[0]

                result = {
                    "msg": "OK",
                    "found_cnt": I.shape[0],
                }

                create_response(req_no=req_no, result=result)
                response_ebuf.write(Serializer.serialize(I))
                response_ebuf.write(Serializer.serialize(D))
        elif method == 'info':
            # TODO
            params = Serializer.deserialize(ebuf)
            result = {
                'msg': 'OK',
                'collections_info': [
                    {
                        "coll_id": 1122,
                        "dimension": 128,
                        "count": 10_000,
                        "trained": False,
                    }
                ],
                'sysinfo': {
                    "total_ram_bytes": 16 * 1024**3,
                    "free_ram_bytes": 8 * 1024**3,
                    "proc_count": 8,
                    "omp_threads": 8,
                },
            }
            create_response(req_no=req_no, result=result)
        elif method == 'add':
            embs = Serializer.deserialize_np_array(ebuf)
            ids = Serializer.deserialize_np_array(ebuf)
            params = Serializer.deserialize(ebuf)
            dimension = int(embs.shape[0] / ids.shape[0])
            embs = embs.reshape(ids.shape[0], dimension)
            assert embs.shape[0] == ids.shape[0]
            assert len(ids.shape) == 1

            if params.get("coll_id") <= 0:
                error = {'msg': 'incorrect coll_id'}
                create_response(req_no=req_no, error=error)
            else:
                result = {
                    'msg': 'OK',
                    'copies': 1,
                    'normalize_L2': False,
                    'inserted_vecs': embs.shape[0],
                }
                create_response(req_no=req_no, result=result)
        elif method == 'create':
            params = Serializer.deserialize(ebuf)

            if params.get("coll_id") <= 0:
                error = {'msg': 'incorrect coll_id'}
                create_response(req_no=req_no, error=error)
            elif params.get("dimension") <= 0:
                error = {'msg': 'incorrect dimension'}
                create_response(req_no=req_no, error=error)
            else:
                result = {'msg': 'OK', 'copies': 1}
                create_response(req_no=req_no, result=result)
        else:
            params = Serializer.deserialize(ebuf)
            error = {
                'msg': "Unknown method!",
                'code': 255,
            }
            create_response(req_no=req_no, error=error)

        await self._socket.send_multipart(
            [
                client,
                empty,
                response_ebuf.getbuffer(),
            ],
        )

        for t in pending_tasks:
            logging.debug("pending task: %s" % t)

        return None

    async def _handle_task_finish(self, finished_task):
        client, response = await finished_task
        if client is not None:
            logging.debug(
                "Task finished (%s), send resp to client %s: %s",
                finished_task,
                client,
                str(response)[:300],
            )
            await self._socket.send_multipart([client, b'', response])

    async def run(self):
        logging.debug("MockServer is running: %s", self._opts.endpoint)

        loop = asyncio.get_running_loop()
        waiter = loop.create_future()
        pending_tasks = set()
        completed_tasks = []

        def _on_completion(f):
            pending_tasks.remove(f)
            completed_tasks.append(f)
            if not waiter.done():
                waiter.set_result(None)

        def _add_task(awaitable):
            awaitable.add_done_callback(_on_completion)
            pending_tasks.add(awaitable)

        self._recv_task = self._socket.recv_multipart(copy=False)
        _add_task(self._recv_task)

        while pending_tasks:
            await waiter
            while completed_tasks:
                finished_task = completed_tasks.pop()

                if finished_task is self._recv_task:
                    await self._handle_recv_task(pending_tasks)

                    if self._recv_task is None:
                        # cmd quit was receive - do not receive new commands
                        continue

                    self._recv_task = self._socket.recv_multipart(copy=False)
                    _add_task(self._recv_task)

                else:
                    if finished_task.cancelled():
                        continue
                    await self._handle_task_finish(finished_task)

            waiter = loop.create_future()

        self._socket.close(0)
        logging.debug("MockServer finish: %s", self._opts.endpoint)

    async def stop(self):
        logging.debug("Stopping MockServer: %s", self._opts.endpoint)
        socket = self._zmq_ctx.socket(zmq.REQ)
        socket.connect("%s://%s" % (self._opts.transport, self._opts.endpoint))
        await socket.send_multipart([b'quit', b''])
        logging.debug("Sent cmd 'quit'")
        await socket.recv_multipart()
        logging.debug("MockServer stopped: %s", self._opts.endpoint)
