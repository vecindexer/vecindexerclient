#!/usr/bin/env bash

mkdir -p test_data/embs
mkdir -p test_small/embs
for i in `seq 0 9` ; do python utils/generate_embs.py "test_data/embs/batch_"$i".npz" 1000 256 ; done
python utils/generate_embs.py "test_data/sample_embs.npz" 1000 256
python utils/generate_embs.py "test_small/embs/batch_0.npz" 10000 128
python utils/generate_embs.py "test_small/sample_embs.npz" 1000 128
