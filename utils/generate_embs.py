#!/usr/bin/env python
# coding: utf-8

import sys
import numpy as np

if len(sys.argv) > 4:
    dtype = sys.argv[4]
else:
    dtype = 'float32'

data = np.random.rand(int(sys.argv[2]), int(sys.argv[3])).astype(dtype)
np.savez(sys.argv[1], data)
