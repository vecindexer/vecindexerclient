#!/usr/bin/env bash

encoder_config=etc/encoder_configs/dcec_default.yml
out_dir="encoders_directory"

temp_dir="/tmp/tmp_embs_1"

cl=etc/test_configs/test_cluster_state_100_bil.ini

for i in {0..10000}
do
    mkdir -p $temp_dir
    python utils/generate_embs.py $temp_dir"/embs_1K.npz" 1000 1024
    python vecindexerclient/cli.py -cl $cl search --embs_dir $temp_dir --batch_size 1000 --coll_id 2000 use_encoder --encoder_config $encoder_config --encoder dcec_default.pt --set_l_hidden --encoder_input_dimension 1024 --encoder_output_dimension 256 --encoders_dir $out_dir 
    rm -r $temp_dir
    sleep 8
done
