#!/usr/bin/env python
# coding: utf-8

import configparser
from typing import List


class ServiceState:
    def __init__(
        self,
        service_id: str,
        service_name: str,
        service_addr: str,
        port: int,
        tags: set,
        status: set,
    ):
        self.service_id = service_id
        self.service_name = service_name
        self.service_addr = service_addr
        self.port = port
        self.tags = tags
        self.status = status


class ClusterState:
    def get_shard_endpoints(self, return_leaders: bool = False) -> List[str]:
        raise RuntimeError("This method must be implemented")


class ClusterStateFromFileConfig(ClusterState):
    def __init__(self, config_file_path: str):
        self.config_file_path = config_file_path
        self.config = configparser.ConfigParser()
        self.config.read(config_file_path)

    def get_shard_endpoints(self, return_leaders: bool = False) -> List[str]:
        services = list()
        for service_id in self.config.sections():
            services.append(
                ServiceState(
                    service_id,
                    self.config[service_id]['name'],
                    self.config[service_id]['address'],
                    self.config[service_id].get('port', None),
                    set(self.config[service_id].get('tags', '').split(',')),
                    self.config[service_id].get('status', 'passing'),
                )
            )

        if return_leaders:
            return [
                f"{s.service_addr}:{s.port}" if s.port else s.service_addr
                for s in services
                if "leader" in s.tags
            ]
        else:
            return [f"{s.service_addr}:{s.port}" if s.port else s.service_addr for s in services]
