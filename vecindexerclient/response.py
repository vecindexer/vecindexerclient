#!/usr/bin/env python
# coding: utf-8

from typing import Union, List
import numpy as np

from .cbor2_extra_serializer import Cbor2ExtraSerializer as Serializer
from .utils import timeit_class_function

from .request_response_convention import ensure_serialized
from .request_response_convention import ensure_deserialized


class Response(object):
    def __init__(
        self,
        req_no: Union[int, str],
        result: Union[dict, str] = None,
        error: Union[dict, str] = None,
        extra: Union[dict, str] = None,
        *args,
        **kwargs,
    ):
        assert isinstance(req_no, (int, str))
        assert result is None or isinstance(result, (dict, str))
        assert error is None or isinstance(error, (dict, str))
        assert extra is None or isinstance(extra, (dict, str))

        self.req_no = req_no
        self.result = result
        self.error = error
        self.extra = extra

    @classmethod
    @timeit_class_function
    def prepare_dump(
        cls,
        req_no: Union[int, str],
        result: Union[dict, str] = None,
        error: Union[dict, str] = None,
        extra: Union[dict, str] = None,
    ):
        response = {'req_no': req_no}
        if result:
            response.update({'result': result})
        if error:
            response.update({'error': error})
        if extra:
            response.update({'extra': extra})
        return response

    @classmethod
    @timeit_class_function
    def prepare(
        cls,
        req_no: Union[int, str],
        result: Union[dict, str] = None,
        error: Union[dict, str] = None,
        extra: Union[dict, str] = None,
    ):
        response = Response.prepare_dump(req_no, result, error, extra)
        return Response(**response)

    def dump(self):
        return Response.prepare_dump(self.req_no, self.result, self.error, self.extra)

    @classmethod
    @timeit_class_function
    def load(cls, data):
        assert isinstance(data, (dict,))
        return cls(**data)

    @classmethod
    @timeit_class_function
    def serialize(cls, response):
        return Serializer.serialize(ensure_serialized(response.dump()))

    @classmethod
    @timeit_class_function
    def deserialize_buffer(cls, buffer, opts=None):
        deserialize_func = Serializer.deserialize_np_array
        if opts:
            try:
                if opts.array_type == 'numpy':
                    deserialize_func = Serializer.deserialize_np_array
                elif opts.array_type == 'array':
                    deserialize_func = Serializer.deserialize_array
                else:
                    raise RuntimeError(f"Incorrect param array_type: {opts.array_type}")
            except RuntimeError as rerr:
                raise rerr
            except Exception as err:
                pass
        return ensure_deserialized(deserialize_func(buffer))

    @classmethod
    @timeit_class_function
    def load_from_buffer(cls, buffer, opts=None):
        return Response(**Response.deserialize_buffer(buffer, opts))


class SearchResponse(Response):
    found_ids: list
    found_distances: list
    parents: list

    @classmethod
    @timeit_class_function
    def prepare_dump(
        cls,
        req_no: Union[int, str],
        result: Union[dict, str] = None,
        error: Union[dict, str] = None,
        extra: Union[dict, str] = None,
        found_ids: Union[list, np.ndarray] = None,
        parents: List = None,
        found_distances: Union[list, np.ndarray] = None,
    ):
        response = {'req_no': req_no}
        if result:
            response.update({'result': result})
        if error:
            response.update({'error': error})
        if extra:
            response.update({'extra': extra})
        if found_ids is not None:
            response.update({'found_ids': found_ids})
        if parents is not None:
            response.update({'parents': parents})
        if found_distances is not None:
            response.update({'found_distances': found_distances})
        return response

    def dump(self):
        return SearchResponse.prepare_dump(
            self.req_no,
            self.result,
            self.error,
            self.extra,
            self.found_ids,
            self.parents,
            self.found_distances,
        )
