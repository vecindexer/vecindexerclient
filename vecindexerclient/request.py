#!/usr/bin/env python
# coding: utf-8

from typing import Union

from .cbor2_extra_serializer import Cbor2ExtraSerializer as Serializer
from .utils import timeit_class_function

from .request_response_convention import ensure_serialized
from .request_response_convention import ensure_deserialized


class Request(object):
    def __init__(self, req_no: Union[int, str], params: dict, extra: dict = None):
        assert isinstance(req_no, (int, str))
        assert isinstance(params, (dict,))
        assert extra is None or isinstance(extra, (dict,))

        self.req_no = req_no
        self.params = params
        if extra:
            self.extra = extra

    @classmethod
    @timeit_class_function
    def prepare_dump(cls, req_no: Union[int, str], params: dict, extra: dict = None):
        request = {
            'req_no': req_no,
            'params': params,
        }
        if extra:
            request.update(
                {
                    'extra': extra,
                }
            )
        return request

    @classmethod
    @timeit_class_function
    def prepare(cls, req_no: Union[int, str], params: dict, extra: dict = None):
        request = Request.prepare_dump(req_no, params, extra)
        return Request(**request)

    def dump(self):
        if hasattr(self, 'extra'):
            extra = self.extra
        else:
            extra = None
        return Request.prepare_dump(self.req_no, self.params, extra)

    @classmethod
    @timeit_class_function
    def parse(cls, data):
        assert isinstance(data, (dict,))
        return Request(**data)

    @classmethod
    @timeit_class_function
    def serialize(cls, request):
        return Serializer.serialize(ensure_serialized(request.dump()))

    @classmethod
    @timeit_class_function
    def load_from_buffer(cls, buffer, opts=None):
        deserialize_func = Serializer.deserialize_np_array
        if opts:
            try:
                if opts.array_type == 'numpy':
                    deserialize_func = Serializer.deserialize_np_array
                elif opts.array_type == 'array':
                    deserialize_func = Serializer.deserialize_array
                else:
                    raise RuntimeError(f"Incorrect param array_type: {opts.array_type}")
            except RuntimeError as rerr:
                raise rerr
            except Exception as err:
                pass

        return Request(**ensure_deserialized(deserialize_func(buffer)))
