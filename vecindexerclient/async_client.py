#!/usr/bin/env python
# coding: utf-8

import asyncio
import copy

from io import BytesIO
import logging
import numpy as np
from typing import Dict, List, MutableMapping, AnyStr, Tuple, Union
import zmq
import zmq.asyncio

from .response import Response, SearchResponse
from .request import Request
from .cbor2_extra_serializer import Cbor2ExtraSerializer as Serializer

from .utils import TimeProfiler


class _AsyncResult:
    """Async Result"""

    def __init__(self, method: AnyStr = None, response: Response = None):
        self.method = method
        self.response = response

    def ready(self):
        return self.response is not None

    def __str__(self):
        return str(self.response)


class AsyncClient:
    """Async client - for one endpoint."""

    def __init__(self, opts, zmq_ctx=None):
        self._opts = opts
        self._zmq_ctx = zmq_ctx
        if zmq_ctx is None:
            self._zmq_ctx = zmq.asyncio.Context()

        self._address = "%s://%s" % (opts.transport, opts.endpoint)
        self._socket = self._zmq_ctx.socket(zmq.DEALER)
        self._socket.connect(self._address)

        self._result_dict: MutableMapping[int, _AsyncResult] = {}

        self._recv_task = None
        self._recv_loop = asyncio.create_task(self._receive_loop())

        self._cond_on_receive = asyncio.Condition()

        logging.debug("AsyncClient inited: %s", self._address)

    async def _receive_loop(self):
        self._recv_task = self._socket.recv_multipart(copy=False)

        pending_tasks = {self._recv_task}

        while pending_tasks:
            done, pending_tasks = await asyncio.wait(
                pending_tasks, return_when=asyncio.FIRST_COMPLETED
            )

            logging.debug("Some tasks completed:\n%s\n%s", done, pending_tasks)

            while done:
                done_task = done.pop()
                if done_task is self._recv_task:
                    if self._recv_task.cancelled():
                        logging.debug("recv_task '%s'cancelled!" % self._recv_task)
                        continue

                    raw_data = await self._recv_task
                    new_task = asyncio.create_task(self._process_new_resp(raw_data))
                    pending_tasks.add(new_task)

                    self._recv_task = self._socket.recv_multipart(copy=False)
                    pending_tasks.add(self._recv_task)
                else:
                    try:
                        await done_task
                    except Exception as e:
                        logging.exception("Failed: %s", e)

        self._socket.close(0)

    async def _process_new_resp(self, raw_data: Tuple):
        _, reply = raw_data

        reply_buf = BytesIO(reply)

        with TimeProfiler(f"AsyncClient: loading response with len {len(reply.buffer)}"):
            resp_dict_or_req_no = Response.deserialize_buffer(reply_buf, self._opts)
            if isinstance(resp_dict_or_req_no, (dict,)):
                response = Response.load(resp_dict_or_req_no)
            else:
                req_no = resp_dict_or_req_no
                result = Response.deserialize_buffer(reply_buf, self._opts)
                response = Response(
                    req_no=req_no,
                    result=result,
                )

        if (
            self._result_dict[response.req_no].method == 'search'
            and response.result
            and 'found_cnt' in response.result
        ):
            response_ = SearchResponse(
                response.req_no,
                response.result,
                response.error,
                response.extra,
            )
            try:
                response_.found_ids = Serializer.deserialize_np_array(reply_buf)
                response_.found_distances = Serializer.deserialize_np_array(reply_buf)
                response = response_
            except Exception as e:
                logging.error(f"Failed to load SearchResponse[{response.req_no}]: {str(e)}")

        logging.debug("new response[:300]: %s", str(response)[:300])

        async def _finalize():
            async with self._cond_on_receive:
                self._cond_on_receive.notify_all()

        self._result_dict[response.req_no].response = response

        await _finalize()

    async def _make_request(self, req_no: Union[int, str], method: AnyStr, parts):
        self._result_dict[req_no] = _AsyncResult(method=method)

        await self._socket.send_multipart(parts)

        async with self._cond_on_receive:
            await self._cond_on_receive.wait_for(self._result_dict[req_no].ready)

        response = self._result_dict[req_no].response
        del self._result_dict[req_no]
        return response

    async def _default_make_request(
        self, cmd: AnyStr, req_no: Union[int, str], params: Dict = None
    ):
        if params is None:
            params = dict()
        request = Request(req_no=req_no, params=params)

        parts = [
            b'',
            bytes(cmd, 'utf-8'),
            Request.serialize(request),
        ]
        logging.debug(
            "Sending request: '%s', '%s'",
            cmd,
            str(request)[:300],
        )
        return await self._make_request(req_no, cmd, parts)

    async def make_request(
        self, cmd: AnyStr, req_no: Union[int, str], params: Dict = None, *args, **kwargs
    ):
        if cmd in ['search', 'train']:
            vecs = kwargs['vecs']
            return await self.make_with_embs_request(
                cmd, req_no, params, embs=vecs, *args, **kwargs
            )
        elif cmd in ['create', 'info', 'manage_coll']:
            return await self.make_base_request(cmd, req_no, params, *args, **kwargs)
        elif cmd == 'add':
            # Maybe bad way to store np arrays in dict
            keys = params.pop('np_array_keys')
            vecs = params.pop('np_array_vecs')
            return await self.make_add_request(
                cmd, req_no, params, embs=vecs, ids=keys, *args, **kwargs
            )
        else:
            # If not special function, then use old-default behavior
            return await self._default_make_request(cmd, req_no, params)

    async def make_base_request(
        self, method: AnyStr, req_no: Union[int, str], params: Dict, *args, **kwargs
    ):
        ebuf = BytesIO()
        ebuf.write(Serializer.serialize(method))
        ebuf.write(Serializer.serialize(str(req_no)))
        if params is None:
            params = dict()

        ebuf.write(Serializer.serialize(params))
        logging.debug(
            "Sending request %s: '%s', '%s'",
            method,
            req_no,
            str(params)[:300],
        )
        return await self._make_request(
            req_no,
            method,
            [
                b'',
                ebuf.getbuffer(),
            ],
        )

    async def make_add_request(
        self,
        method: AnyStr,
        req_no: Union[int, str],
        params: Dict,
        embs: np.ndarray,
        ids: np.ndarray,
        *args,
        **kwargs,
    ):
        assert ids.dtype == np.uint64

        ebuf = BytesIO()
        ebuf.write(Serializer.serialize(method))
        ebuf.write(Serializer.serialize(str(req_no)))
        ebuf.write(Serializer.serialize(embs))
        ebuf.write(Serializer.serialize(ids))
        ebuf.write(Serializer.serialize(params))
        logging.debug(
            "Sending request %s: '%s', '%s'",
            method,
            req_no,
            str(params)[:300],
        )
        return await self._make_request(
            req_no,
            method,
            [
                b'',
                ebuf.getbuffer(),
            ],
        )

    async def make_with_embs_request(
        self,
        method: AnyStr,
        req_no: Union[int, str],
        params: Dict,
        embs: np.ndarray,
        *args,
        **kwargs,
    ):

        ebuf = BytesIO()
        ebuf.write(Serializer.serialize(method))
        ebuf.write(Serializer.serialize(str(req_no)))
        ebuf.write(Serializer.serialize(embs.shape[0]))
        ebuf.write(Serializer.serialize(embs))
        ebuf.write(Serializer.serialize(params))
        logging.debug(
            "Sending request %s: '%s', '%s'",
            method,
            req_no,
            str(params)[:300],
        )
        return await self._make_request(
            req_no,
            method,
            [
                b'',
                ebuf.getbuffer(),
            ],
        )

    async def stop(self, wait: bool = True):
        logging.debug("Stopping AsyncClient: %s", self._address)
        if self._recv_task:
            self._recv_task.cancel()
            if wait:
                await self._recv_loop

        logging.debug("AsyncClient stopped: %s", self._address)


class ShardedAsyncClient:
    """
    Async client for shards (few endpoints)
    """

    def __init__(self, opts, shard_endpoints: List = None, zmq_ctx=None):
        self._opts = opts
        self._zmq_ctx = zmq_ctx
        if zmq_ctx is None:
            self._zmq_ctx = zmq.asyncio.Context()

        self.shard_endpoints = []
        self.async_clients = dict()

        if shard_endpoints is not None:
            self.shard_endpoints = shard_endpoints

    async def _init_async_clients(self, shard_endpoints: List = None):
        if shard_endpoints is None:
            shard_endpoints = self.shard_endpoints
        elif shard_endpoints is not None and set(shard_endpoints) != set(self.shard_endpoints):
            if len(self.async_clients) > 0:
                logging.debug("Stoping old clients!")

            for _, cl in self.async_clients.items():
                await cl.stop()

            if len(self.async_clients) > 0:
                logging.debug(f"Stoped {len(self.async_clients)} clients!")

        if set([endp for endp in self.async_clients.keys()]) == set(shard_endpoints):
            return

        self.shard_endpoints = shard_endpoints
        self.async_clients = dict()
        for endpoint in shard_endpoints:
            shard_opts = copy.copy(self._opts)
            shard_opts.endpoint = endpoint
            self.async_clients[endpoint] = AsyncClient(shard_opts, self._zmq_ctx)

        logging.debug(f"Created {len(self.async_clients)} async_clients")

    async def make_common_sharded_request(
        self,
        cmd: AnyStr,
        req_no: Union[int, str],
        params: Dict = None,
        shard_endpoints: List = None,
        *args,
        **kwargs,
    ):
        await self._init_async_clients(shard_endpoints)

        if len(self.async_clients) == 0:
            logging.error("There is no async_clients!")
            return None

        tasks = [
            cl.make_request(cmd, req_no=req_no, params=params, *args, **kwargs)
            for _, cl in self.async_clients.items()
        ]
        results = await asyncio.gather(*tasks)
        return results

    async def make_sharded_request(
        self, cmd: AnyStr, shards_params_generator, shard_endpoints: List = None
    ):
        await self._init_async_clients(shard_endpoints)

        if len(self.async_clients) == 0:
            logging.error("There is no async_clients!")
            return []

        tasks = [
            self.async_clients[endpoint].make_request(cmd, req_no=req_no, params=shard_params)
            for req_no, shard_params, endpoint in shards_params_generator
        ]
        results = await asyncio.gather(*tasks)
        return results

    async def stop(self, wait: bool = True):
        logging.debug("Stopping ShardedAsyncClient!")
        for _, cl in self.async_clients.items():
            await cl.stop(wait)
        logging.debug("ShardedAsyncClient stopped!")
