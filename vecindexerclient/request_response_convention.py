#!/usr/bin/env python
# coding: utf-8

import numpy as np


numpy_arrays_keys_starts_with = [
    "<class 'numpy.ndarray'>",
    'numpy.ndarray',
    'np.ndarray',
    'np.array',
    'numpy.array',
    'np_array',
    'numpy_array',
    'ndarray',
    'np_ndarray',
    'numpy_ndarray',
]


def ensure_deserialized_numpy_arrays(data):
    """
    Numpy ndarrays looks like (before):
    {
        'ndarray':
        {
           'type': 'float32',
           'shape': [100, 16],
           'array': [1, 2, 3, ...],
        }
    }
    Numpy ndarrays looks like (after):
    {
        'ndarray': [1, 2, 3, ...], # numpy.ndarray
    }
    :param data:
    :return:
    """

    if not isinstance(data, (dict,)):
        return data

    for k in data.keys():
        if any([k.startswith(i) for i in numpy_arrays_keys_starts_with]):
            try:
                if set(data[k].keys()) != {'type', 'shape', 'array'}:
                    raise Exception("This is not our case!")

                dtype = data[k]['type']
                shape = data[k]['shape']
                assert str(data[k]['array'].dtype) == dtype
                data[k] = data[k]['array'].reshape(shape)
            except Exception as e:
                pass

        if isinstance(data[k], (dict,)):
            data[k] = ensure_deserialized_numpy_arrays(data[k])

    return data


def ensure_serialized_numpy_arrays(data):
    """
    Numpy ndarrays looks like (before):
    {
        'ndarray': [1, 2, 3, ...], # numpy.ndarray
    }
    Numpy ndarrays looks like (after):
    {
        'ndarray':
        {
           'type': 'float32',
           'shape': [100, 16],
           'array': [1, 2, 3, ...],
        }
    }
    :param data:
    :return:
    """

    if not isinstance(data, (dict,)):
        return data

    for k in data.keys():
        if any([k.startswith(i) for i in numpy_arrays_keys_starts_with]):
            try:
                if not isinstance(data[k], np.ndarray):
                    raise Exception("This is not our case!")

                dtype = str(data[k].dtype)
                shape = data[k].shape
                data[k] = {
                    'type': dtype,
                    'shape': shape,
                    'array': data[k],
                }
            except Exception as e:
                pass

        if isinstance(data[k], (dict,)):
            data[k] = ensure_serialized_numpy_arrays(data[k])

    return data


def ensure_deserialized(data):
    for f in [
        ensure_deserialized_numpy_arrays,
    ]:
        data = f(data)
    return data


def ensure_serialized(data):
    for f in [
        ensure_serialized_numpy_arrays,
    ]:
        data = f(data)
    return data
