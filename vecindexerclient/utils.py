#!/usr/bin/env python
# coding: utf-8

import argparse
import logging
from functools import wraps
import time
import os

DEBUG_SEC_THRESHOLD = int(os.environ.get("DEBUG_SEC_THRESHOLD", 5))


class TimeProfiler(object):
    def __init__(self, name=None):
        self.name = name

    def __enter__(self, name=None):
        if name is not None:
            self.name = name

        self.start = time.time()

    def __exit__(self, type, value, traceback):
        t = time.time() - self.start
        if t > DEBUG_SEC_THRESHOLD:
            logging.debug("TimeProfiler. {}: {:.2f} sec".format(self.name, t))


def _base_timeit(func, cls=None, message_begin="Function took", *args, **kwargs):
    start_time = time.perf_counter()
    if cls is None:
        result = func(*args, **kwargs)
    else:
        result = func(cls, *args, **kwargs)
    end_time = time.perf_counter()
    total_time = end_time - start_time
    if total_time > DEBUG_SEC_THRESHOLD:
        logging.debug(f'{message_begin} {total_time:.4f} seconds')
    return result


def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        return _base_timeit(
            func, cls=None, message_begin=f'Function {func.__name__} Took', *args, **kwargs
        )

    return timeit_wrapper


def timeit_class_function(func):
    @wraps(func)
    def timeit_wrapper(cls, *args, **kwargs):
        return _base_timeit(
            func, cls, f'Class function {cls.__name__}.{func.__name__} Took', *args, **kwargs
        )

    return timeit_wrapper


class ArgsFormatter(argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter):
    pass
