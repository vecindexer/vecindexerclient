#!/usr/bin/env python
# coding: utf-8

import numpy as np
from typing import Dict, List, Union
import asyncio
import faiss
import logging
import uuid

from .async_client import ShardedAsyncClient
from .cluster_state import ClusterStateFromFileConfig as ClusterState
from .sharding_strategy import ShardingStrategy
from .response import SearchResponse
from .utils import TimeProfiler


def generate_req_id():
    return str(uuid.uuid1())


class VecIndexer(ShardedAsyncClient):
    def __init__(
        self, opts, cluster_state: ClusterState, sharding_strategy: ShardingStrategy, zmq_ctx=None
    ):
        self.cluster_state = cluster_state
        self.sharding_strategy = sharding_strategy

        super().__init__(opts, self.get_cluster_instances(), zmq_ctx)

    def get_cluster_instances(self, leaders: bool = True):
        return self.cluster_state.get_shard_endpoints(return_leaders=leaders)

    async def create(self, params: Dict):
        return await self.make_common_sharded_request(
            'create',
            generate_req_id(),
            params,
            self.get_cluster_instances(),
        )

    async def info(self, params: Dict):
        return await self.make_common_sharded_request(
            'info',
            generate_req_id(),
            params,
            self.get_cluster_instances(),
        )

    def _ensure_vecs(self, vecs: np.ndarray):
        if vecs.dtype != np.float32:
            logging.warning(f"vecs dtype is {vecs.dtype} ! Changing to float32")
            vecs = np.float32(vecs)

        assert vecs.dtype == np.float32
        return vecs

    async def train(self, vecs: np.ndarray, params: Dict):
        vecs = self._ensure_vecs(vecs)

        shard_endpoints = self.cluster_state.get_shard_endpoints(return_leaders=True)
        return await self.make_common_sharded_request(
            'train', generate_req_id(), params, shard_endpoints, vecs=vecs
        )

    async def add(self, keys: Union[List[int], np.ndarray], vecs: np.ndarray, params: Dict):
        if isinstance(keys, np.ndarray):
            assert keys.dtype == np.uint64
            keys_cnt = keys.shape[0]
        elif isinstance(keys, list):
            keys_cnt = len(keys)
        else:
            raise RuntimeError("Incorrect keys!")

        assert keys_cnt == vecs.shape[0]

        vecs = self._ensure_vecs(vecs)

        shard_endpoints = self.get_cluster_instances()
        shard_indices = self.sharding_strategy.shard_for_keys(keys, len(shard_endpoints))

        assert keys_cnt == len(shard_indices)

        shard_requests = {}
        for keys_idx, vec_idx, shard_idx in zip(
            range(keys_cnt), range(vecs.shape[0]), shard_indices
        ):
            if shard_idx not in shard_requests:
                shard_requests[shard_idx] = {'keys_idx': [], 'vec_idx': []}
            shard_requests[shard_idx]['keys_idx'].append(keys_idx)
            shard_requests[shard_idx]['vec_idx'].append(vec_idx)

        def _shard_options_generator():
            for shard_idx, shard_req in shard_requests.items():
                shard_params = dict()
                shard_params.update(params)

                if isinstance(shard_req['keys_idx'], np.ndarray):
                    shard_params['np_array_keys'] = keys[shard_req['keys_idx']]
                elif isinstance(shard_req['keys_idx'], list):
                    shard_params['np_array_keys'] = np.array(
                        [keys[i] for i in shard_req['keys_idx']], np.uint64
                    )
                else:
                    raise RuntimeError("Incorrect keys_idx!")

                assert shard_params['np_array_keys'].dtype == np.uint64

                shard_params['np_array_vecs'] = vecs[shard_req['vec_idx']]

                assert (
                    shard_params['np_array_keys'].shape[0] == shard_params['np_array_vecs'].shape[0]
                )

                yield generate_req_id(), shard_params, shard_endpoints[shard_idx]

        responses = await self.make_sharded_request(
            'add', _shard_options_generator(), shard_endpoints
        )

        return responses

    async def add_multi_level(
        self, keys: Union[List[int], np.ndarray], vecs: np.ndarray, params: Dict
    ):
        if not ('multi_level' in params and params['multi_level']):
            return self.add(keys, vecs, params)

        if isinstance(keys, np.ndarray):
            assert keys.dtype == np.uint64
            keys_cnt = keys.shape[0]
        elif isinstance(keys, list):
            keys_cnt = len(keys)
        else:
            raise RuntimeError("Incorrect keys!")

        vecs = self._ensure_vecs(vecs)

        assert keys_cnt == 3 * vecs.shape[0]

        vecs_cnt = vecs.shape[0]

        keys_l1 = keys[:vecs_cnt]
        keys_l2 = keys[vecs_cnt : vecs_cnt * 2]
        keys_l3 = keys[vecs_cnt * 2 :]

        shard_endpoints = self.get_cluster_instances()
        shard_indices = self.sharding_strategy.shard_for_keys(keys_l3, len(shard_endpoints))

        assert keys_cnt == 3 * len(shard_indices)

        shard_requests = {}
        for keys_idx, vec_idx, shard_idx in zip(
            range(vecs_cnt), range(vecs.shape[0]), shard_indices
        ):
            if shard_idx not in shard_requests:
                shard_requests[shard_idx] = {'keys_idx': [], 'vec_idx': []}
            shard_requests[shard_idx]['keys_idx'].append(keys_idx)
            shard_requests[shard_idx]['vec_idx'].append(vec_idx)

        def _shard_options_generator():
            for shard_idx, shard_req in shard_requests.items():
                shard_params = dict()
                shard_params.update(params)

                if isinstance(shard_req['keys_idx'], np.ndarray):
                    shard_params['np_array_keys'] = np.concatenate(
                        (
                            keys_l1[shard_req['keys_idx']],
                            keys_l2[shard_req['keys_idx']],
                            keys_l3[shard_req['keys_idx']],
                        ),
                        axis=None,
                    )
                elif isinstance(shard_req['keys_idx'], list):
                    shard_params['np_array_keys'] = np.array(
                        [keys_l1[i] for i in shard_req['keys_idx']]
                        + [keys_l2[i] for i in shard_req['keys_idx']]
                        + [keys_l3[i] for i in shard_req['keys_idx']],
                        np.uint64,
                    )
                else:
                    raise RuntimeError("Incorrect keys_idx!")

                assert shard_params['np_array_keys'].dtype == np.uint64

                shard_params['np_array_vecs'] = vecs[shard_req['vec_idx']]

                assert (
                    shard_params['np_array_keys'].shape[0]
                    == 3 * shard_params['np_array_vecs'].shape[0]
                )

                yield generate_req_id(), shard_params, shard_endpoints[shard_idx]

        responses = await self.make_sharded_request(
            'add', _shard_options_generator(), shard_endpoints
        )

        return responses

    async def search(self, vecs: np.ndarray, params: Dict):
        shard_endpoints = self.cluster_state.get_shard_endpoints(return_leaders=True)

        vecs = self._ensure_vecs(vecs)

        results = await self.make_common_sharded_request(
            'search', generate_req_id(), params, shard_endpoints, vecs=vecs
        )
        nq = vecs.shape[0]
        topk = params.get('topk', 10)

        result_heap = faiss.ResultHeap(nq=nq, k=topk)
        result_heap_ok = False
        with TimeProfiler("reshape + result_heap"):
            for result in results:
                if isinstance(result, SearchResponse):
                    result_heap_ok = True
                    result.found_distances = np.array(result.found_distances, np.float32).reshape(
                        (nq, topk)
                    )
                    result.found_ids = np.array(result.found_ids, np.uint64).reshape((nq, topk))
                    result_heap.add_result(D=result.found_distances, I=result.found_ids)
                else:
                    logging.error("Search result not SearchResponse: %s", result)

            result_heap.finalize()

        return {
            "found": result_heap.I.shape[0] if result_heap_ok else 0,
            "found_ids": result_heap.I if result_heap_ok else 0,
            "found_distances": result_heap.D if result_heap_ok else 0,
            "raw_results": results,
        }

    async def search_multi_level(self, vecs: np.ndarray, params: Dict):
        if not ('parents' in params and params['parents']):
            return self.search(vecs, params)

        shard_endpoints = self.cluster_state.get_shard_endpoints(return_leaders=True)

        vecs = self._ensure_vecs(vecs)

        results = await self.make_common_sharded_request(
            'search', generate_req_id(), params, shard_endpoints, vecs=vecs
        )
        nq = vecs.shape[0]
        topk = params.get('topk', 10)

        result_heap = faiss.ResultHeap(nq=nq, k=topk)
        result_heap_ok = False

        id2parents = dict()
        # {1: (l1, l2),
        #  2: (l3, l4),
        # }

        with TimeProfiler("reshape + result_heap"):
            for result in results:
                if isinstance(result, SearchResponse):
                    result_heap_ok = True
                    result.found_distances = np.array(result.found_distances, np.float32).reshape(
                        (nq, topk)
                    )
                    assert result.found_ids.shape[0] == 3 * nq * topk

                    level1 = result.found_ids[: nq * topk]
                    level2 = result.found_ids[nq * topk : 2 * nq * topk]
                    level3 = result.found_ids[2 * nq * topk :]

                    for l1_id, l2_id, frg_id in zip(level1, level2, level3):
                        id2parents[frg_id] = (l1_id, l2_id)

                    result.found_ids = np.array(level3, np.uint64).reshape((nq, topk))

                    result_heap.add_result(D=result.found_distances, I=result.found_ids)
                else:
                    logging.error("Search result not SearchResponse: %s", result)

            result_heap.finalize()

        parents = np.zeros((result_heap.I.shape[0], topk, 2), dtype=np.uint64)
        for i in range(result_heap.I.shape[0]):
            parents[i] = np.array([id2parents[frg_id] for frg_id in result_heap.I[i]])

        if result_heap_ok:
            return {
                "found": result_heap.I.shape[0],
                "found_ids": result_heap.I,
                "parents": parents,
                "found_distances": result_heap.D,
                "raw_results": results,
            }
        else:
            return {
                "error": "Failed to merge results!",
                "raw_results": results,
            }

    async def manage_coll(self, params: Dict):
        return await self.make_common_sharded_request(
            'manage_coll',
            generate_req_id(),
            params,
            self.get_cluster_instances(),
        )

    async def search_by_id(self, keys: List[int]):
        # No ready yet!
        vectors = await self.get_vectors(keys)
        return await self.search(vectors)

    async def get_vectors(self, keys: List[int]):
        # No ready yet!
        shard_endpoints = self.cluster_state.get_shard_endpoints(return_leaders=False)
        shard_indices = self.sharding_strategy.shard_for_keys(keys, len(shard_endpoints))

        shard_requests = {}
        for key, shard_idx in zip(keys, shard_indices):
            if shard_idx not in shard_requests:
                shard_requests[shard_idx] = {'keys': []}
            shard_requests[shard_idx]['keys'].append(key)

        tasks = [
            self.make_request(
                'get_vectors', key, {'keys': shard_req['keys']}, endpoint=shard_endpoints[shard_idx]
            )
            for shard_idx, shard_req in shard_requests.items()
        ]
        responses = await asyncio.gather(*tasks)

        vectors = [None] * len(keys)
        for response in responses:
            for key, vec in zip(response['keys'], response['vecs']):
                idx = keys.index(key)
                vectors[idx] = vec

        return np.array(vectors)
