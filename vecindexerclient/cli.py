#!/usr/bin/env python
# coding: utf-8


from autofaiss.external.build import estimate_memory_required_for_index_creation
from autofaiss.external.metadata import IndexMetadata

import argparse
import asyncio
import faiss
import uvloop
from dataclasses import dataclass

import logging
import numpy as np
import os.path as fs
import sys
import torch


from vecindexerclient.vecindexer import VecIndexer
from vecindexerclient.sharding_strategy import ShardingStrategy
from vecindexerclient.cluster_state import ClusterStateFromFileConfig as ClusterState

from vecindexerclient.utils import ArgsFormatter
from vecindexerclient.utils import TimeProfiler

from vecindexerclient.helpers import (
    str2bool,
    Profiler,
    embs_files_generator,
    TrainEncoderTask,
    EmbsReader,
    encode,
    init_data_loader,
)


asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


@dataclass
class SampleDefaultOpts:
    verbose: bool = True
    embs_dir: str = 'embs'
    sample_embs: str = 'sample_embs.npz'
    normalize_embs: bool = False


@dataclass
class FaissBaseOpts:
    no_precomputed_tables: bool = False
    usefloat16: bool = False
    max_points_per_centroid: int = 256
    nprobe: int = 20


@dataclass
class TrainOpts:
    train_embs: str = 'train_sample.npz'


@dataclass
class EvaluateDefaultOpts:
    ground_truth_I: str = None
    input_i: str = None


@dataclass
class IndexingOpts(SampleDefaultOpts, FaissBaseOpts, TrainOpts, EvaluateDefaultOpts):
    top: int = 10
    metric: str = 'L2'
    gpu: bool = False
    batch_size: int = 100_000
    index: str = "IDMap,HNSW32"


@dataclass
class EncoderTrainOpts(TrainOpts):
    epochs: int = 5
    encoder_batch_size: int = 8
    set_l_hidden: bool = True


@dataclass
class EncoderBaseOpts:
    encoder_config: str = 'ae_configs/dcec_default.yml'
    encoder: str = 'dcec_default.pt'
    encoders_dir: str = 'encoders_directory'
    encoder_input_dimension: int = 512
    encoder_output_dimension: int = 64
    normalize_input_vectors: bool = False
    normalize_output_vectors: bool = False


@dataclass
class EncodeAndFaissOpts(IndexingOpts, EncoderTrainOpts, EncoderBaseOpts):
    pass


@dataclass
class ClientBaseOpts:
    transport: str = 'tcp'
    consul_endpoint: str = 'localhost:8500'
    array_type: str = 'numpy'


class ClientOpts(ClientBaseOpts):
    pass


@dataclass
class CollectionOpts(EncodeAndFaissOpts):
    coll_id: int = None
    batch_size: int = 100_000


@dataclass
class CreateCollectionOpts(CollectionOpts):
    dimension: int = 1024
    estimated_vecs_count: int = 1_000_000
    force_create: bool = False
    use_gpu: bool = False


@dataclass
class AddOpts(CollectionOpts):
    ids_files_dir: str = None
    ids_starts_from: int = 0
    multi_level: bool = False


@dataclass
class SearchOpts(CollectionOpts):
    parents: bool = False
    pass


@dataclass
class TrainOpts(CollectionOpts):
    pass


@dataclass
class InfoOpts:
    collections: list = None


def add_base_coll_args(parser):
    o = CollectionOpts()
    parser.add_argument("--coll_id", type=int, required=True, help="collection id")


def add_create_coll_args(parser):
    o = CreateCollectionOpts()
    add_base_coll_args(parser)
    parser.add_argument("--index", type=str, help="index description (faiss index-factory string)")
    parser.add_argument(
        "--dimension",
        type=int,
        required=True,
        help="dimension. Be careful when used encoder (need setup encoder output dim)",
    )
    parser.add_argument(
        "--estimated_vecs_count", type=int, required=True, help="estimated vecs count"
    )
    parser.add_argument("--force_create", action="store_true", help="set if need force create")
    parser.add_argument("--use_gpu", action="store_true", help="try to move to gpu after creation")


def _add_encoder_args(encoder_parser, o):
    encoder_parser.add_argument(
        "--encoders_dir", default=o.encoders_dir, type=str, help="encoders out dir"
    )
    encoder_parser.add_argument("--train_embs", default=o.train_embs, type=str, help="")
    encoder_parser.add_argument(
        "--encoder_config", default=o.encoder_config, type=str, help="path for encoder config"
    )
    encoder_parser.add_argument("--encoder", default=o.encoder, type=str, help="encoder filename")
    encoder_parser.add_argument(
        "--normalize_input_vectors",
        default=o.normalize_input_vectors,
        type=str2bool,
        nargs='?',
        const=True,
        help="",
    )
    encoder_parser.add_argument(
        "--normalize_output_vectors",
        default=o.normalize_output_vectors,
        type=str2bool,
        nargs='?',
        const=True,
        help="",
    )
    encoder_parser.add_argument(
        "--set_l_hidden", default=o.set_l_hidden, type=str2bool, nargs='?', const=True, help=""
    )
    encoder_parser.add_argument("--epochs", default=o.epochs, type=int, help="")
    encoder_parser.add_argument(
        "--encoder_batch_size", type=int, default=o.encoder_batch_size, help=""
    )
    encoder_parser.add_argument(
        "--encoder_input_dimension", default=o.encoder_input_dimension, type=int, help=""
    )
    encoder_parser.add_argument(
        "--encoder_output_dimension", default=o.encoder_output_dimension, type=int, help=""
    )


def add_train_args(parser):
    o = TrainOpts()
    add_base_coll_args(parser)
    parser.add_argument("--train_embs", default=o.train_embs, type=str, help="train embs file")

    subparsers = parser.add_subparsers(help='sub-command help')
    encoder_parser = subparsers.add_parser('use_encoder', help='use encoder')
    _add_encoder_args(encoder_parser, o)


def add_vectors_add_args(parser):
    o = AddOpts()
    parser.add_argument(
        "--embs_dir",
        type=str,
        default=o.embs_dir,
        help="directory with .npz files with embeddings",
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=o.batch_size,
        help="batch size",
    )
    parser.add_argument(
        "--ids_files_dir",
        type=str,
        default=o.ids_files_dir,
        help="directory with .npz files with ids",
    )
    parser.add_argument(
        "--ids_starts_from",
        type=int,
        default=o.ids_starts_from,
        help="ids starts from (use this if not set 'ids_files_dir' param)",
    )
    parser.add_argument(
        "--multi_level",
        type=bool,
        default=o.multi_level,
        help="if using multi-level system",
    )
    parser.add_argument(
        "--generate_embs",
        action="store_true",
        help="generate embs",
    )
    parser.add_argument(
        "--generate_embs_cnt",
        type=int,
        default=0,
        help="generate embs count",
    )
    parser.add_argument("--generate_dimension", default=1024, type=int, help="")
    add_base_coll_args(parser)

    subparsers = parser.add_subparsers(help='sub-command help')
    encoder_parser = subparsers.add_parser('use_encoder', help='use encoder')
    _add_encoder_args(encoder_parser, o)


def add_search_args(parser):
    o = SearchOpts()
    parser.add_argument(
        "--embs_dir",
        type=str,
        default=o.embs_dir,
        help="directory with .npz files with embeddings",
    )
    parser.add_argument(
        "--parents",
        type=str,
        default=o.parents,
        help="find parent docs for vectors",
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=o.batch_size,
        help="batch size",
    )
    add_base_coll_args(parser)

    subparsers = parser.add_subparsers(help='sub-command help')
    encoder_parser = subparsers.add_parser('use_encoder', help='use encoder')
    _add_encoder_args(encoder_parser, o)


def add_manage_coll_args(parser):
    add_base_coll_args(parser)

    parser.add_argument("--move_to_gpu", action="store_true", help="")
    parser.add_argument("--move_to_cpu", action="store_true", help="")


async def create_coll_cli(vecindexer, opts):
    cluster_info = await vecindexer.info({})

    if cluster_info is None:
        raise RuntimeError("cluster is not ok!")

    def _debug_info():
        for resp in cluster_info:
            if resp.result:
                logging.debug(resp.dump())

    for resp in cluster_info:
        if not resp.result or 'collections_info' not in resp.result or 'sysinfo' not in resp.result:
            logging.error("Failed to retrieve info from shards. Debug below (use -v).")
            _debug_info()
            return

    available_free_ram_bytes = sys.maxsize
    available_total_ram_bytes = sys.maxsize
    collection_already_created = 0
    shards_count = len(cluster_info)
    vecs_per_shard = int(opts.estimated_vecs_count / shards_count)
    for resp in cluster_info:
        for colls_info in resp.result['collections_info']:
            if opts.coll_id == colls_info['coll_id']:
                collection_already_created += 1

        free_ram_bytes = resp.result['sysinfo']['free_ram_bytes']
        total_ram_bytes = resp.result['sysinfo']['total_ram_bytes']
        if free_ram_bytes > 0:
            available_free_ram_bytes = min(free_ram_bytes, available_free_ram_bytes)

        if total_ram_bytes > 0:
            available_total_ram_bytes = min(total_ram_bytes, available_total_ram_bytes)

    if available_free_ram_bytes == sys.maxsize or available_total_ram_bytes == sys.maxsize:
        logging.error("Failed to get available ram. Debug below (use -v).")
        _debug_info()
        return

    if collection_already_created == shards_count:
        logging.info("Collection already exists. Nothing to do. Debug below (use -v).")
        _debug_info()
        return
    elif collection_already_created > 0:
        logging.error(
            "Collection created in part of shards! Fix it manually! Debug below (use -v)."
        )
        _debug_info()
        return

    def _recommended_index(available_bytes):
        return estimate_memory_required_for_index_creation(
            nb_vectors=vecs_per_shard,
            vec_dim=opts.dimension,
            max_index_memory_usage=str(available_bytes) + 'B',
        )

    if opts.index:
        estimated_index_size_in_bytes = IndexMetadata(
            opts.index, vecs_per_shard, opts.dimension, True
        ).estimated_index_size_in_bytes()
        index = opts.index
    else:
        rec_index_est_bytes, rec_index = _recommended_index(available_free_ram_bytes)
        estimated_index_size_in_bytes = IndexMetadata(
            rec_index, vecs_per_shard, opts.dimension, True
        ).estimated_index_size_in_bytes()
        logging.info(
            f"(autofaiss) recommended index type (on free ram): {rec_index}, size: {estimated_index_size_in_bytes}"
        )
        if not opts.force_create:
            logging.info(
                "Use --force_create for force create recommended index type. Debug below (use -v)."
            )
            _debug_info()
            return
        index = rec_index

    if estimated_index_size_in_bytes <= 0:
        logging.error("Failed to estimate index size.")
        if not opts.force_create:
            logging.info("Use --force_create for force create. Debug below (use -v).")
            _debug_info()
            return
    else:
        message = None
        if available_free_ram_bytes < estimated_index_size_in_bytes:
            message = f"available_free_ram_bytes({available_free_ram_bytes}) < estimated_index_size_in_bytes({estimated_index_size_in_bytes})."
            logging.warning(message)
        if available_total_ram_bytes < estimated_index_size_in_bytes:
            message = f"available_total_ram_bytes({available_total_ram_bytes}) < estimated_index_size_in_bytes({estimated_index_size_in_bytes})."
            logging.warning(message)
        if message:
            rec_index_total_ram_est_bytes, rec_index_total_ram = _recommended_index(
                available_total_ram_bytes
            )
            rec_index_total_ram_est_store_bytes = IndexMetadata(
                rec_index_total_ram, vecs_per_shard, opts.dimension, True
            ).estimated_index_size_in_bytes()
            rec_index_free_ram_est_bytes, rec_index_free_ram = _recommended_index(
                available_free_ram_bytes
            )
            rec_index_free_ram_est_store_bytes = IndexMetadata(
                rec_index_free_ram, vecs_per_shard, opts.dimension, True
            ).estimated_index_size_in_bytes()

            assert rec_index_total_ram_est_bytes < available_total_ram_bytes
            assert rec_index_total_ram_est_store_bytes < available_total_ram_bytes
            assert rec_index_free_ram_est_bytes < available_total_ram_bytes
            assert rec_index_free_ram_est_store_bytes < available_total_ram_bytes

            logging.info(
                "(autofaiss) recommened index type (on total ram): %s (in moment: %sB, final size"
                ": %sB)",
                rec_index_total_ram,
                rec_index_total_ram_est_bytes,
                rec_index_total_ram_est_store_bytes,
            )
            logging.info(
                "(autofaiss) recommened index type (on free ram): %s (in moment: %sB, final size: %sB)",
                rec_index_free_ram,
                rec_index_free_ram_est_bytes,
                rec_index_free_ram_est_store_bytes,
            )
            if not opts.force_create:
                logging.info("Use --force_create for force create. Debug below (use -v).")
                _debug_info()
                return

    params = {
        "coll_id": opts.coll_id,
        "index": index,
        "dimension": opts.dimension,
        "use_gpu": opts.use_gpu,
    }
    return await vecindexer.create(params)


async def info_cli(vecindexer, opts):
    if opts.collections:
        params = {
            "collections": opts.collections,
        }
    else:
        params = {}
    return await vecindexer.info(params)


async def train_cli(vecindexer, opts):
    params = {
        "coll_id": opts.coll_id,
    }

    encoder = None
    if hasattr(opts, 'encoder'):
        tet = TrainEncoderTask(opts, None)
        if not tet():
            raise RuntimeError("Train encoder task failed!")

        encoder_path = fs.join(opts.encoders_dir, opts.encoder)
        assert fs.exists(encoder_path)

        encoder = torch.load(encoder_path)
        encoder.encoder.eval()
        encoder.decoder.eval()

    if encoder:
        with Profiler('read embs and encode') as p:
            embs = encode(opts, opts.train_embs, encoder)
    else:
        with Profiler('read embs') as p:
            embs = EmbsReader(opts).read(opts.train_embs)
            logging.debug(f"Loaded embs {embs.shape}. Example first vector: {embs[0]}")

    return await vecindexer.train(vecs=embs, params=params)


async def _add_cli(vecindexer, opts):
    encoder = None
    if hasattr(opts, 'encoder'):
        tet = TrainEncoderTask(opts, None)
        if not tet():
            raise RuntimeError("Train encoder task failed!")

        encoder_path = fs.join(opts.encoders_dir, opts.encoder)
        assert fs.exists(encoder_path)

        encoder = torch.load(encoder_path)
        encoder.encoder.eval()
        encoder.decoder.eval()

    added_cnt = 0
    result = list()
    ids_files = None
    if opts.ids_files_dir is not None:
        ids_files = [i for i in embs_files_generator(opts.ids_files_dir)]

    embs_files = [e for e in embs_files_generator(opts.embs_dir)]
    if ids_files is not None:
        assert len(embs_files) == len(ids_files)

    for i, embs_filename in enumerate(embs_files):
        ids_file = None
        if ids_files is not None:
            ids_file = ids_files[i]

        with Profiler(f"Add {embs_filename}") as p:
            if encoder:
                with Profiler('read embs and encode') as p:
                    embs = encode(opts, embs_filename, encoder)
            else:
                with Profiler('read embs') as p:
                    embs = EmbsReader(opts).read(embs_filename)
                    logging.debug(f"Loaded embs {embs.shape}. Example first vector: {embs[0]}")

            params = {
                "coll_id": opts.coll_id,
            }
            if ids_file is not None:
                with Profiler('read ids') as p:
                    ids = EmbsReader(opts).read(ids_file)
            else:
                assert opts.ids_starts_from >= 0
                ids = np.array(
                    [
                        i + 1
                        for i in range(
                            opts.ids_starts_from + added_cnt,
                            opts.ids_starts_from + added_cnt + embs.shape[0],
                        )
                    ],
                    np.uint64,
                )

            logging.debug(f"Ids {ids.shape}. Example first vector: {ids[0]}")

            assert ids.shape[0] == embs.shape[0]

            added_cnt += len(ids)

            current_shift = 0
            batch_size = opts.batch_size
            with Profiler('vecindexer add') as p:
                res_ = list()
                while current_shift < embs.shape[0]:
                    if opts.multi_level:
                        res = await vecindexer.add(
                            keys=ids[current_shift : current_shift + batch_size * 3],
                            vecs=embs[current_shift : current_shift + batch_size],
                            params=params,
                        )
                        res_.append(res)
                        current_shift += batch_size
                    else:
                        res = await vecindexer.add(
                            keys=ids[current_shift : current_shift + batch_size],
                            vecs=embs[current_shift : current_shift + batch_size],
                            params=params,
                        )
                        res_.append(res)
                        current_shift += batch_size

                logging.info(f"Added {embs_filename}. Res: {res_}")
                result.extend(res_)

    return result


async def _add_generated_vectors_cli(vecindexer, opts):
    encoder = None
    if hasattr(opts, 'encoder'):
        tet = TrainEncoderTask(opts, None)
        if not tet():
            raise RuntimeError("Train encoder task failed!")

        encoder_path = fs.join(opts.encoders_dir, opts.encoder)
        assert fs.exists(encoder_path)

        encoder = torch.load(encoder_path)
        encoder.encoder.eval()
        encoder.decoder.eval()

    added_cnt = 0
    result = list()

    batch_size = opts.batch_size
    for i in range(int(opts.generate_embs_cnt / batch_size)):
        embs = np.random.rand(batch_size, opts.generate_dimension).astype('float32')
        if encoder:
            with Profiler('encode') as p:
                logging.info(f"embs shape: {embs.shape}")
                with Profiler('init data loader') as p:
                    data_loader = init_data_loader(embs, opts.encoder_batch_size)

                with Profiler('apply encoder') as p:
                    embs = encoder.apply_encoder(data_loader)
                    if opts.normalize_output_vectors:
                        faiss.normalize_L2(embs)

                    logging.debug(f"Example first encoded embs: {embs[0]}")

        params = {
            "coll_id": opts.coll_id,
        }

        assert opts.ids_starts_from >= 0
        ids = np.array(
            [
                i + 1
                for i in range(
                    opts.ids_starts_from + added_cnt,
                    opts.ids_starts_from + added_cnt + embs.shape[0],
                )
            ],
            np.uint64,
        )

        logging.debug(f"Ids {ids.shape}. Example first vector: {ids[0]}")

        assert ids.shape[0] == embs.shape[0]

        with Profiler('vecindexer add') as p:
            res = await vecindexer.add(
                keys=ids,
                vecs=embs,
                params=params,
            )
            result.append(res)
            added_cnt += len(ids)

    return result


async def add_cli(vecindexer, opts):
    if opts.generate_embs and opts.generate_embs_cnt > 0:
        return await _add_generated_vectors_cli(vecindexer, opts)
    else:
        return await _add_cli(vecindexer, opts)


async def search_cli(vecindexer, opts):
    encoder = None
    if hasattr(opts, 'encoder'):
        tet = TrainEncoderTask(opts, None)
        if not tet():
            raise RuntimeError("Train encoder task failed!")

        encoder_path = fs.join(opts.encoders_dir, opts.encoder)
        assert fs.exists(encoder_path)

        encoder = torch.load(encoder_path)
        encoder.encoder.eval()
        encoder.decoder.eval()

    result = list()
    for embs_filename in embs_files_generator(opts.embs_dir):
        with Profiler(f"Search {embs_filename}") as p:
            if encoder:
                with Profiler('read embs and encode') as p:
                    embs = encode(opts, embs_filename, encoder)
            else:
                with Profiler('read embs') as p:
                    embs = EmbsReader(opts).read(embs_filename)
                    logging.debug(f"Loaded embs {embs.shape}. Example first vector: {embs[0]}")

            params = {
                "coll_id": opts.coll_id,
            }

            current_shift = 0
            batch_size = opts.batch_size
            with Profiler('index search') as p:
                res_ = list()
                while current_shift < embs.shape[0]:
                    res = await vecindexer.search(
                        vecs=embs[current_shift : current_shift + batch_size],
                        params=params,
                    )

                    res_out = {
                        'found': res['found'],
                        'found_ids_shape': (
                            res['found_ids'].shape
                            if 'found_ids' in res and hasattr(res['found_ids'], 'shape')
                            else None
                        ),
                        'found_dist_shape': (
                            res['found_distances'].shape
                            if 'found_distances' in res and hasattr(res['found_distances'], 'shape')
                            else None
                        ),
                    }
                    logging.info(f"Search finished(batch): {embs_filename}. Res: {res_out}")

                    res_.append(res)
                    current_shift += batch_size

                logging.debug(f"Searched {embs_filename}. Res: {res_}")
                result.extend(res_)

    return result


async def manage_coll_cli(vecindexer, opts):
    with Profiler('manage collection') as p:
        params = {
            'coll_id': opts.coll_id,
            'move_to_gpu': opts.move_to_gpu,
            'move_to_cpu': opts.move_to_cpu,
        }
        return await vecindexer.manage_coll(params)


def add_async_client_opts(parser):
    o = ClientOpts()
    parser.add_argument(
        "--transport", type=str, default=o.transport, help="transport (tcp or other)"
    )
    parser.add_argument("--endpoint", '-s', default=o.consul_endpoint, help="consul endpoint")
    parser.add_argument(
        "--default_array_type", default=o.array_type, help="default array type (numpy or array)"
    )

    subparsers = parser.add_subparsers(help="sub-command help")

    info_parser = subparsers.add_parser(
        'info',
        help="retrieve info",
    )
    info_parser.add_argument(
        "--collections", type=int, nargs="+", default=[], required=False, help="collection ids"
    )
    info_parser.set_defaults(func=info_cli)

    create_coll_parser = subparsers.add_parser(
        'create_coll',
        help="create collection",
    )
    add_create_coll_args(create_coll_parser)
    create_coll_parser.set_defaults(func=create_coll_cli)

    train_parser = subparsers.add_parser(
        'train',
        help="train",
    )
    add_train_args(train_parser)
    train_parser.set_defaults(func=train_cli)

    add_parser = subparsers.add_parser(
        'add',
        help="add vectors",
    )
    add_vectors_add_args(add_parser)
    add_parser.set_defaults(func=add_cli)

    search_parser = subparsers.add_parser(
        'search',
        help="search vectors",
    )
    add_search_args(search_parser)
    search_parser.set_defaults(func=search_cli)

    manage_coll_parser = subparsers.add_parser(
        'manage_coll',
        help="manage collection parser",
    )
    add_manage_coll_args(manage_coll_parser)

    manage_coll_parser.set_defaults(func=manage_coll_cli)


async def main():
    parser = argparse.ArgumentParser(
        prog="VecIndexerClient.",
        description="""
        """,
        formatter_class=ArgsFormatter,
    )
    parser.add_argument("--verbose", "-v", action="store_true", default=False, help="")
    parser.add_argument("--cluster_state_file", "-cl", required=True, type=str, help="")

    add_async_client_opts(parser)
    args = parser.parse_args()

    FORMAT = "%(asctime)s %(levelname)s: %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, format=FORMAT)
    logging.debug("Args: %s", args)

    if not hasattr(args, 'func'):
        parser.print_help()
        return

    try:
        with TimeProfiler("whole process") as p:
            vecindexer = VecIndexer(
                args,
                ClusterState(args.cluster_state_file),
                ShardingStrategy(),
            )

            def _print_result(result):
                for res in result:
                    if hasattr(res, 'dump'):
                        logging.info(res.dump())
                    elif isinstance(res, list):
                        _print_result(res)

            result = await args.func(vecindexer, args)
            if isinstance(result, list):
                _print_result(result)
            else:
                logging.info("Whole result: %s", result)

            await vecindexer.stop(True)
    except Exception as e:
        logging.exception("failed to run: %s ", e)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
