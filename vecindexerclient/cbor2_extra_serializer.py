#!/usr/bin/env python
# coding: utf-8

import cbor2

import pycbor_extra.typed_arrays as typed_arrays

from .utils import timeit_class_function


class Cbor2ExtraSerializer(object):
    def __init__(self):
        pass

    @classmethod
    @timeit_class_function
    def serialize(cls, data):
        return cbor2.dumps(data, default=typed_arrays.default)

    @classmethod
    @timeit_class_function
    def deserialize(cls, bytes_data):
        return cbor2.load(bytes_data)

    @classmethod
    @timeit_class_function
    def deserialize_array(cls, bytes_data):
        return cbor2.load(bytes_data, tag_hook=typed_arrays.arr_hook)

    @classmethod
    @timeit_class_function
    def deserialize_np_array(cls, bytes_data):
        return cbor2.load(bytes_data, tag_hook=typed_arrays.numpy_hook)
