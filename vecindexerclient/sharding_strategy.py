#!/usr/bin/env python
# coding: utf-8

from typing import List


class ShardingStrategy:
    def shard_for_key(self, key: int, nshards: int) -> int:
        return int(key) % nshards

    def shard_for_keys(self, keys: List[int], nshards: int) -> List[int]:
        return [self.shard_for_key(key, nshards) for key in keys]
