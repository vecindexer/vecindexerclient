import argparse
from enum import Enum
import glob
import logging
import numpy as np
import os
import os.path as fs
import time
import yaml

import faiss
from faiss.contrib.datasets import fvecs_read, ivecs_read

import torch
from torch.utils.data import DataLoader
from autoencoders import get_ae
from autoencoders.dataset import EmbeddingDataset


# https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class Profiler(object):
    def __init__(self, name=None):
        self.name = name

    def __enter__(self, name=None):
        if name is not None:
            self.name = name

        self.start = time.time()

    def __exit__(self, type, value, traceback):
        logging.debug("PROFILER. {}: {:.3f} sec".format(self.name, time.time() - self.start))


def embs_files_generator(d):
    return sorted([f for f in glob.glob(fs.join(d, '*')) if fs.isfile(f)])


class SimilarityMetrics(Enum):
    L2 = ['l2', 'euclidean']
    IP = ['ip', 'inner-product', 'innerproduct']

    @staticmethod
    def is_euclidean(metric):
        return metric.lower() in SimilarityMetrics.L2.value

    @staticmethod
    def is_inner_product(metric):
        return metric.lower() in SimilarityMetrics.IP.value


# https://gist.github.com/CharlesLiu7/0496773a8a21934a746e3fd496f11e0c
def bvecs_read(fname):
    a = np.fromfile(fname, dtype=np.int32, count=1)
    b = np.fromfile(fname, dtype=np.uint8)
    d = a[0]
    return b.reshape(-1, d + 4)[:, 4:].copy()


def read_raw_embs(fname, key='arr_0'):
    if fname.endswith('.npz'):
        with np.load(fname) as _data:
            return _data[key]
    elif fname.endswith('.fvecs'):
        return fvecs_read(fname)
    elif fname.endswith('.ivecs'):
        return ivecs_read(fname)
    elif fname.endswith('.bvecs'):
        return bvecs_read(fname)
    else:
        raise RuntimeError(f"Unknown extension of {fname}")


def read_normalized_embs(fname, *args, **kwargs):
    embs = read_raw_embs(fname, *args, **kwargs)
    faiss.normalize_L2(embs)
    return embs


def read_embs(fname, key='arr_0'):
    return read_raw_embs(fname, key=key)


class EmbsReader(object):
    def __init__(self, opts):
        self.opts = opts

    def read(self, fname):
        normalized = False
        if hasattr(self.opts, 'normalize_embs'):
            if self.opts.normalize_embs is True:
                normalized = True
            else:
                normalized = False

        if hasattr(self.opts, 'metric') and SimilarityMetrics.is_inner_product(self.opts.metric):
            normalized = True

        if normalized:
            logging.debug(f"Read normalized embs {fname}")
            return read_normalized_embs(fname)
        else:
            logging.debug(f"Read raw embs {fname}")
            return read_raw_embs(fname)


def _read_embs(opts, filename):
    if opts.normalize_input_vectors:
        with Profiler(f'read normalized embs {filename}') as p:
            embs = read_normalized_embs(filename)
    else:
        with Profiler(f'read raw embs {filename}') as p:
            embs = read_raw_embs(filename)

    return embs


def init_data_loader(embs, batch_size):
    dataset = EmbeddingDataset(embs)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=False)
    return data_loader


class Task(object):
    name: str

    def __init__(self, opts, ns, *args, **kwargs):
        self.opts = opts
        self.ns = ns

    def __call__(self, *args, **kwargs) -> bool:
        raise RuntimeError("Must be implemented!")


class TrainEncoderTask(Task):
    name = 'train encoder'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if not fs.exists(self.opts.encoders_dir):
            os.makedirs(self.opts.encoders_dir)

        encoder_path = fs.join(self.opts.encoders_dir, self.opts.encoder)
        if fs.exists(encoder_path):
            return True

        with Profiler('init model') as p:
            model_config = load_model_config(self.opts)
            encoder = get_ae(**model_config["model_cfg"])

        embs = _read_embs(self.opts, self.opts.train_embs)
        logging.debug(f"Got train embs {embs.shape}")
        logging.debug(f"Example first embs: {embs[0]}")

        with Profiler('init data loader') as p:
            data_loader = init_data_loader(embs, self.opts.encoder_batch_size)

        with Profiler('train') as p:
            encoder.train(data_loader, self.opts.epochs)

        with Profiler('save model') as p:
            torch.save(encoder, encoder_path)

        return fs.exists(encoder_path)


def load_model_config(opts):
    assert hasattr(opts, 'encoder_config')
    assert fs.exists(opts.encoder_config)
    assert hasattr(opts, 'encoder_input_dimension')
    assert hasattr(opts, 'encoder_output_dimension')

    with open(opts.encoder_config) as f:
        model_config = yaml.safe_load(f)

    assert 'model_cfg' in model_config
    assert 'x_dim' in model_config['model_cfg']

    model_config['model_cfg']['x_dim'] = opts.encoder_input_dimension
    model_config['model_cfg']['z_dim'] = opts.encoder_output_dimension

    x_dim = model_config['model_cfg']['x_dim']
    z_dim = model_config['model_cfg']['z_dim']

    if z_dim > (x_dim / 2):
        raise RuntimeError(f"Check please x_dim {x_dim} and z_dim {z_dim}")

    for submodel in ['encoder', 'decoder']:
        for l_hidden in ['l_hidden']:
            if (
                submodel in model_config['model_cfg']
                and l_hidden in model_config['model_cfg'][submodel]
            ):
                old = model_config['model_cfg'][submodel][l_hidden]

                new = [z_dim]

                if old and hasattr(opts, 'set_l_hidden') and opts.set_l_hidden:
                    model_config['model_cfg'][submodel][l_hidden] = new
                    logging.info(f"replaced {l_hidden}: {old} -> {new}")

    return model_config


def encode_embs(opts, embs, encoder):
    logging.info(f"embs shape: {embs.shape}")
    with Profiler('init data loader') as p:
        data_loader = init_data_loader(embs, opts.encoder_batch_size)

    with Profiler('apply encoder') as p:
        encoded_embs = encoder.apply_encoder(data_loader)
        if opts.normalize_output_vectors:
            faiss.normalize_L2(encoded_embs)

        logging.debug(f"Example first encoded embs: {encoded_embs[0]}")

        return encoded_embs


def encode(opts, embs_filename, encoder):
    embs = _read_embs(opts, embs_filename)
    logging.debug(f"Example first for-encode embs: {embs[0]}")
    return encode_embs(opts, embs, encoder)
