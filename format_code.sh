#!/usr/bin/env bash

for d in vecindexerclient tests examples;
do
  black -t py37 -l 100 -S  $(find $d -name '*.py' -type f)
done ;

