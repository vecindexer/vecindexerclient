import sqlite3
# import faiss

class Add:
    # Функция добавления документа
    def add_document(conn, content, metadata=None):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO documents (content, metadata) VALUES (?, ?)", (content, metadata))
        conn.commit()
        return cursor.lastrowid

    # Функция добавления фрагмента уровня 1
    def add_fragment_l1(conn, doc_id, content, metadata=None):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO fragments_l1 (doc_id, content, metadata) VALUES (?, ?, ?)", (doc_id, content, metadata))
        conn.commit()
        return cursor.lastrowid

    # Функция добавления фрагмента уровня 2
    def add_fragment_l2(conn, frg_l1_id, content, metadata=None):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO fragments_l2 (frg_l1_id, content, metadata) VALUES (?, ?, ?)", (frg_l1_id, content, metadata))
        conn.commit()
        return cursor.lastrowid

    # Функция добавления вектора
    def add_vector(conn, uvid, doc_id, frg_l1_id, frg_l2_id, vector_data):
        cursor = conn.cursor()
        cursor.execute("INSERT INTO vectors (uvid, doc_id, frg_l1_id, frg_l2_id, vector_data) VALUES (?, ?, ?, ?, ?)",
                    (uvid, doc_id, frg_l1_id, frg_l2_id, vector_data))
        conn.commit()


class Find:
    # Функция поиска документа по doc_id
    def find_document_by_id(conn, doc_id):
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM documents WHERE doc_id=?", (doc_id,))
        return cursor.fetchone()

    # Функция поиска фрагмента уровня 1 по frg_l1_id
    def find_fragment_l1_by_id(conn, frg_l1_id):
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM fragments_l1 WHERE frg_l1_id=?", (frg_l1_id,))
        return cursor.fetchone()

    # Функция поиска фрагмента уровня 2 по frg_l2_id
    def find_fragment_l2_by_id(conn, frg_l2_id):
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM fragments_l2 WHERE frg_l2_id=?", (frg_l2_id,))
        return cursor.fetchone()

    # Функция поиска вектора по uvid
    def find_vector_by_uvid(conn, uvid):
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM vectors WHERE uvid=?", (uvid,))
        return cursor.fetchone()
