import sqlite3


def __create_connection__(database):
    conn = None
    try:
        conn = sqlite3.connect(database)
    except sqlite3.Error as e:
        print(e)
    finally:
        return conn


def __create_tables__(conn):
    cursor = conn.cursor()

    # Создание таблицы documents
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS documents (
        doc_id INTEGER PRIMARY KEY,
        content BLOB,
        metadata TEXT
    )
    """)

    # Создание таблицы fragments_l1
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS fragments_l1 (
        frg_l1_id INTEGER PRIMARY KEY,
        doc_id INTEGER,
        content BLOB,
        metadata TEXT,
        FOREIGN KEY (doc_id) REFERENCES documents (doc_id)
    )
    """)

    # Создание таблицы fragments_l2
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS fragments_l2 (
        frg_l2_id INTEGER PRIMARY KEY,
        frg_l1_id INTEGER,
        content BLOB,
        metadata TEXT,
        FOREIGN KEY (frg_l1_id) REFERENCES fragments_l1 (frg_l1_id)
    )
    """)

    # Создание таблицы vectors
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS vectors (
        uvid INTEGER PRIMARY KEY,
        doc_id INTEGER,
        frg_l1_id INTEGER,
        frg_l2_id INTEGER,
        vector_data BLOB,
        FOREIGN KEY (doc_id) REFERENCES documents (doc_id),
        FOREIGN KEY (frg_l1_id) REFERENCES fragments_l1 (frg_l1_id),
        FOREIGN KEY (frg_l2_id) REFERENCES fragments_l2 (frg_l2_id)
    )
    """)

    # Применить изменения в базе данных
    conn.commit()


def setup_db(db_name: str = "multimodal.db") -> None:
    # Создание и настройка базы данных
    conn = __create_connection__(db_name)
    if conn is not None:
        __create_tables__(conn)
    else:
        print("Error! Cannot create the database connection.")
