from setuptools import setup, find_packages

import os
from pathlib import Path


setup(
    name='vecindexerclient',
    version='0.1.0',
    description='vecindexer client',
    packages=find_packages(),
    install_requires=[
        # main deps
        'autofaiss',
        'cbor2',
        'numpy',
        'pytest',
        'pytest_asyncio',
        'pyzmq',
        'uvloop',
        'zmq',

        # autoencoders deps
        'torch',
        'scikit-learn',
        'tqdm',
        'faiss-cpu',
        'pyyaml',
    ]
)


def _install_from_git(url, package_name):
    command = f"if python -c 'import {package_name}' ; then echo Skip ; else git clone {url} && cd {Path(url).stem} && pip install . && cd - && rm -rf {Path(url).stem} ; fi "
    os.system(command)


_install_from_git('https://github.com/dvzubarev/pycbor_extra.git',
                  'pycbor_extra')

_install_from_git('https://github.com/Astromis/autoencoders.git',
                  'autoencoders')
